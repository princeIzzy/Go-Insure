const express = require('express'),
    session = require('cookie-session'),
    rateLimit = require('express-rate-limit'),
    bodyParser = require('body-parser'),
    helmet = require('helmet'),
    mongoSanitize = require('express-mongo-sanitize'),
    xss = require('xss-clean'),
    hpp = require('hpp'),
    cors = require('cors');

const user = require('./routes/user'),
    payment = require('./routes/payment'),
    product = require('./routes/product'),
    service = require('./routes/service'),
    subscription = require('./routes/subscription'),
    dashboardEntity = require('./routes/dashboardEntities'),
    companyEntities = require('./routes/companyEntities'),
    settlementHistory = require('./routes/settlementHistory'),
    bank = require('./routes/bank'),
    //logo_upload = require('./routes/user'),
    globalErrHandler = require('./controllers/errorController'),
    AppError = require('./utils/appError'),
    app = express(),
    swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger');

var Bugsnag = require('@bugsnag/js');
var BugsnagPluginExpress = require('@bugsnag/plugin-express');
    
Bugsnag.start({
    apiKey: '206ddb5c21adada7fd7d79dce3991fe4',
    plugins: [BugsnagPluginExpress]
})

var middleware = Bugsnag.getPlugin('express')

const IN_PROD = process.env.NODE_ENV === 'production'

// This must be the first piece of middleware in the stack.
// It can only capture errors in downstream middleware
app.use(middleware.requestHandler)

app.use(express.static('uploads'));
// Body parser, reading data from body into req.body
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(session({
    name: process.env.SESS_NAME,
    keys: ['key1', 'keys2'],
    resave: false,
    saveUninitialized: false,
    secret: process.env.SESS_SECRET,
    cookie: {
        maxAge: 1000*60*60*2,
        sameSite: true,
        secure: IN_PROD
    }
}));

// Allow Cross-Origin requests
app.use(cors());

// Set security HTTP headers
app.use(helmet());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// Limit request from the same API 
const limiter = rateLimit({
    max: 150,
    windowMs: 60 * 60 * 100,
    message: 'Too Many Request from this IP, please try again in an hour'
});
app.use('/api', limiter);

// Data sanitization against Nosql query injection
app.use(mongoSanitize());
//
// Data sanitization against XSS(clean user input from malicious HTML code)
app.use(xss());

// Prevent parameter pollution
app.use(hpp());


// Routes
app.use('/api/v1/home', (req, res, next) =>{
    res.render('welcome to my home page');
    next();
});
app.use('/api/v1/contant_us', (req, res, next) =>{
    res.render('contact us using the contacts below for more info');
    next();
});
app.use('/api/v1/payment', payment);
app.use('/api/v1/product', product);
app.use('/api/v1/service', service);
app.use('/api/v1/subscription', subscription);
app.use('/api/v1/user', user);
app.use('/api/v1/dashboardEntities', dashboardEntity);
app.use('/api/v1/companyEntities', companyEntities);
app.use('/api/v1/settlementHistory', settlementHistory);
app.use('/api/v1/bank', bank);

// This handles any errors that Express catches. This needs to go before other
// error handlers. Bugsnag will call the `next` error handler if it exists.
app.use(middleware.errorHandler)

// handle undefined Routes
app.use('*', (req, res, next) => {
    const err = new AppError(404, 'fail', 'undefined route');
    next(err, req, res, next);
});

app.use(globalErrHandler);

module.exports = app;