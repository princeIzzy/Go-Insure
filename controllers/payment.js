const Payment = require('../models/payment'),
        Service = require('../models/service'),
        Subscription = require('../models/subscription'),
        SettlementHistory = require('../models/settlementHistory'),
        User = require('../models/user'),
        Email = require('../services/mail'),
        {roleConstant} = require('../constants/index'),
        base = require('./baseController'),
        axios = require('axios'),  
        {validationResult} = require('express-validator'),
        AppError = require('../utils/appError');

exports.createPayment = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    
    try {
        const user = await User.findById(req.body.userID);
        if(!user){
            return next(new AppError(422, 'fail', 'Invalid UserID'), req, res, next)
        };

        const company = await User.findById(req.body.companyID);
        if(!company){
            return next(new AppError(422, 'fail', 'Invalid companyID'), req, res, next)
        };

        const payment = await Payment.findOne({subscriptionID: req.body.subscriptionID, userID: req.body.userID});
        if(payment && payment.subscriptionID && payment.userID){
          return next(new AppError(422, 'fail', 'User already made payment for this subscription'), req, res, next);
        };

        const subscription = await Subscription.findByIdAndUpdate({_id:req.body.subscriptionID}, {isPaid: true, period: req.body.period});
        if(!subscription){
          return next(new AppError(422, 'fail', 'User is yet to subscribe'), req, res, next)
        };
        
        req.body.user_name = user.firstname;
        if(req.body.transactionID) {
            const transactionDetails = await verifyTransaction(req.body.transactionID);
            console.log("example ==>", transactionDetails);
        }
        
        const data = await Payment.create(req.body);  
        let updatedData = {};

        if(data) {
            await createSettlementHistory(data._id, req.body.companyID)
            updatedData = await Payment.findByIdAndUpdate(data._id, {$set: {isSettled:true}}, {
                new: true,
                runValidators: true,
            });
        }; 
        
        const emailBody = '<p>Hello '+user.firstname+',</p><p>Thank you for making payment on Go-Insure.</p><p>kindly await your certificate which will be sent to your mail shortly.</p><p>Regards.</p><p>GoInsure Team</p>'; // HTML body
        const subject = 'GoInsure payment';        

        const company_emailBody = '<p>Hello '+company.firstname+',</p><p>This is to notify you that a customer just subscribed to an insurance policy under your company</p><p>Settlement into your account will be completed automatically within 24 hours.</p><p>Regards.</p><p>GoInsure Team</p>'; // HTML body
        const company_subject = 'Payment Notification For New Subscription';
        const goinsure_emailBody = '<p>Hello Admin,</p><p>This is to notify you that a payment was just made on GoInsure</p><p>Settlement into your account will be completed automatically within 24 hours.</p><p>Regards.</p><p>GoInsure Team</p>'; // HTML body


        // Mail User
        await Email.sendMail(user.email, subject, emailBody);
        await Email.sendMail(company.email, company_subject, company_emailBody);    
        await Email.sendMail('accounts@goinsure.ng', company_subject, goinsure_emailBody);        

        res.status(200).json({
            status: 'success',
            updatedData
        });

    } catch (error) {
        next(error);
    }
};

const createSettlementHistory = async (paymentID, companyId) => {
    const payment = await Payment.findById(paymentID);
    const amountPaidByCustomer = parseInt(payment.amount);
    const serviceId = payment.serviceID;
    const service = await Service.findById(serviceId);
    const user = await User.findById(companyId);
    const companyAmount = (parseInt(service.companyRate)/100)*amountPaidByCustomer;

    let doc = {};
        doc.paymentID = paymentID;
        doc.company_amount = companyAmount;
        doc.company_name = user.company_details['company_name'];
        doc.total_amount = amountPaidByCustomer;
        doc.GoInsure_amount = parseInt(doc.total_amount)-parseInt(doc.company_amount);
        
    let settled = await SettlementHistory.create(doc);
        console.log('This payment has been settled ====>', settled);

    return settled;
};

const verifyTransaction = async(id) => {
    try {
        const verifyPaymentTransaction = await axios.get(process.env.FLUTTERWAVE_VERIFY_TRANSACTION+id+"/verify", {
            headers: { 
                "Authorization": process.env.FLUTTERWAVE_TEST_KEY 
            }
        });
    
        return verifyPaymentTransaction;
    
    } catch (error) {
        return error;
    }
};

const populateOption = {
    populate:[{
        path:'userID',
        select: 'firstname lastname',
    },  
    {
        path: 'serviceID',
        select: '_id userID productID isSettled',
        populate:{
            path: 'productID',
            select: 'name'
        }
    }]
}

exports.getAllPayments = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {

        var page = (req.query.page) ? req.query.page : 1;
        var perPage = (req.query.limit) ? req.query.limit :10;

        var options = {
            sort: req.query.sort || {createdAt: -1},  
            populate: populateOption.populate,          
            lean: true,
            page: page, 
            limit: perPage
        };

        const data = await Payment.paginate({}, options);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }

};

exports.getPaymentsByCompany = async (req, res, next) => {

    const user = await User.findById(req.params.userId);

    if(!user) {
        return next(new AppError(404, 'fail', 'No company document found with that id'), req, res, next);
    }
   
    Payment.find().populate([{
        path: 'serviceID',
        select: 'productID userID valueType valueAmount',
        populate:{
            path: 'productID',
            select: 'name'
        }
    },{
        path: 'userID',
        select: 'firstname lastname email',
    }]).exec(function(err, payments) {
        if(err) {
            next(error);
        }
        else {            
            result = payments.filter(payment => {                
                if(payment && payment.serviceID) {
                    return payment.serviceID.userID == req.params.userId;
                }
                
            });     
            res.status(200).json({
                status: 'success',
                result
            });
        }       
    });
};

exports.exportPayments = base.export(Payment);
exports.getPayment = base.getOne(Payment);
exports.updatePayment = base.updateOne(Payment);
exports.deletePayment = base.deleteOne(Payment);
exports.searchPayment = base.search(Payment, populateOption);

exports.advancePaymentSearch = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const service = await Service.findById(req.body.query['serviceID']._id);

        if(!service){
            return next(new AppError(404, 'fail', 'This id doesn\'t exist'), req, res, next);
        }

        var page = (req.body.page) ? req.body.page : 1;
        var perPage = (req.body.limit) ? req.body.limit :10;
        var query = {serviceID: {_id: service._id}, isSettled: req.body.query.isSettled}

        var options = {
            sort: req.body.sort || {createdAt: -1},      
            populate: populateOption.populate,      
            lean: true,
            page: page, 
            limit: perPage
        };
        
        const data = await Payment.paginate(query, options); 

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};
