const UserDocuments = require('../models/user_documents'),        
        AppError = require('../utils/appError');

exports.getUserDocument = async (req, res, next) => {
    try {
        const data = await UserDocuments.findById(req.params.id);

        if (!data) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        res.status(200).json({
            status: 'success',
            data
        });
    } catch (error) {
        next(error);
    }
};

exports.getUserDocuments = async (req, res, next) => {
    try {
        
        const data = await UserDocuments.find({UserID: req.params.id});

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.getAllUserDocuments = async (req, res, next) => {
    try {

        var page = (req.query.page) ? req.query.page : 1;
        var perPage = (req.query.limit) ? req.query.limit :10;

        var options = {
            sort: req.query.sort || {createdAt: -1},            
            lean: true,
            page: page, 
            limit: perPage
        };

        const data = await UserDocuments.paginate({}, options);

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};


/* exports.UploadDocument = async (req, res, next) =>{
    try {
      const errors = validationResult(req);
  
      if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() });
      }
  
      let doc = {};
      const path = req.file && req.file.path;
  
      if(path) {
          doc.field_name = req.body.field_name;
          doc.file = path;
      }
  
      const data = await User.findOneAndUpdate({userID: req.body.userID}, 
          { $push: {documents: doc}}, {new: true, runValidators: true}
      )
  
      if (!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
      };
  
      res.status(200).json({
          status: 'updated',
          user:data
      });
  
  } catch (error) {
      next(error);
  }
  
  }; */