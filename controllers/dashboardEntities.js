const User = require('../models/user'),
    Subscription = require('../models/subscription'),
    Product = require('../models/products'),
    Service = require('../models/service'),
    Payment = require('../models/payment'),
    {validationResult} = require('express-validator');

exports.countEntities = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {

        const Total_Customers = await User.countDocuments({role: 3});
        const Active_Customers = await User.countDocuments({role: 3, isActive: true});
        const Inactive_Customers = await User.countDocuments({role: 3, isActive: false});
        const Total_Companies = await User.countDocuments({role: 2, parentID: null});
        const Active_Companies = await User.find({role: 2, parentID: null, isActive: true}).countDocuments();
        const Inactive_Companies = await User.find({role: 2, parentID: null, isActive: { $in: [false]}}).countDocuments();
        const Total_Admin = await User.countDocuments({role: 1});
        const Active_Admin = await User.countDocuments({role: 1, isActive: true});
        const Inactive_Admin = await User.countDocuments({role: 1, isActive: false});
        const Total_Products = await Product.countDocuments({})
        const Products_with_parent = await Product.countDocuments({has_parent: true});
        const Products_without_parent = await Product.countDocuments({has_parent: false});
        const Services = await Service.countDocuments();
        const Total_Subscriptions = await Subscription.countDocuments();
        const Active_Subscriptions = await Subscription.countDocuments({isActive: true});
        const Inactive_Subscriptions = await Subscription.countDocuments({isActive: false});
        const Total_Certificates = await Subscription.countDocuments({certificateSent: { $in: [true, false]}})
        const Sent_Certificates = await Subscription.countDocuments({certificateSent: { $in: [true]}})
        const Pending_Certificates = await Subscription.countDocuments({certificateSent: { $in: [false]}})
        const Payments = await Payment.countDocuments();

        res.status(200).json({
            status: 'success',
            Customers:{
              Total_Customers,
              Active_Customers,
              Inactive_Customers
            },
            Companies: {
              Total_Companies,
              Active_Companies,
              Inactive_Companies
            },
            Admin: {
              Total_Admin,
              Active_Admin,
              Inactive_Admin
            },
            Products: {
              Total_Products,
              Products_with_parent,
              Products_without_parent
            },
            Services,
            Subscriptions: {
              Total_Subscriptions,
              Active_Subscriptions,
              Inactive_Subscriptions
            },
            Payments,
            Certificate: {
              Total_Certificates,
              Sent_Certificates,
              Pending_Certificates
            }
        });

    } catch (error) {
        next(error);
    }

};