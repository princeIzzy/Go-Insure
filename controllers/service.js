const base = require('./baseController'),
        User = require('../models/user'),
        Subscription = require('../models/subscription'),
        Payment = require('../models/payment'),
        Service = require('../models/service'),
        Product = require('../models/products'),
        {roleConstant} = require('../constants/index'),
        {validationResult} = require('express-validator'),
        AppError = require('../utils/appError'),
        decoder = require('html-entity-decoder');


exports.createService = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    
    try {
        const product = await Product.findById(req.body.productID);
        const user = await User.findById(req.body.userID);

        if(!product) {
          return next(new AppError(422, 'fail', 'This product does not exist.'), req, res, next);
        };

        const service = await Service.findOne({productID: req.body.productID, userID: req.body.userID});
        
        if(service){
          return next(new AppError(422, 'fail', 'This product has already been registered by this company.'), req, res, next);
        };

        req.body.parentID = product.parentID;
        req.body.product_name = product.name;
        req.body.published = false;
        req.body.subAccountID = user.company_details['bank_details'].flutterSubAccount_id;

        let data = await Service.create(req.body);
        data = await data.populate('productID', 'name').execPopulate();                
        
        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.getService = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {
        const data = await Service.findById(req.params.id).populate('productID', 'name description');
        if (!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }        

        res.status(200).json({
            status: 'success',
            data
        });
    } catch (error) {
        next(error);
    }
};

exports.updateService = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {

        if(req.params.id !== req.body._id) {
          return next(new AppError(404, 'fail', 'The id provided doesn\'t match the user id you are trying to access'), req, res, next);
        };
        
        const service = await Service.findById(req.body._id);
        const user = await User.findById(service.userID);
        const userSubAccountID = user.company_details['bank_details'].flutterSubAccount_id;

        if(!service.subAccountID && userSubAccountID) {
            req.body.subAccountID = userSubAccountID
        }

        const data = await Service.findByIdAndUpdate(req.params.id, {$set: req.body}, {
            new: true,
            runValidators: true,
        }).populate('productID', 'name description');

        if (!data) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        };       

        res.status(200).json({
            status: 'updated',
            data
        });

    } catch (error) {
        next(error);
    }
};

const populateOption = {
    populate:[{
        path:'productID',
        select: 'name description',
        
    },  
    {
        path:'userID',
        select: 'company_details.company_name company_details.logo_url',
    }],
}

exports.getAllServices = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {

        var page = (req.query.page) ? req.query.page : 1;
        var perPage = (req.query.limit) ? req.query.limit :10;

        var options = {
            sort: req.query.sort || {createdAt: -1},
            populate: populateOption.populate,
            lean: false,
            page: page, 
            limit: perPage
        };        

        const data = await Service.paginate({published:true}, options);   
        if(!data) {
          return next(new AppError(404, 'fail', 'this page could not be paginated'))
        }

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.deleteService = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const data = await Service.findByIdAndDelete(req.params.id);
        if (!data) {
          return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        const serviceID = req.params.id;

        base.deleteFromRelation(Subscription, {serviceID});
        base.deleteFromRelation(Payment, {serviceID});

        res.status(200).json({
            status: 'This document has been deleted'
        });
    } catch (error) {
        next(error);
    }
};

exports.searchService = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        //console.log("entered here!!")
        var page = (req.body.page) ? req.body.page : 1;
        var perPage = (req.body.limit) ? req.body.limit :10;
        var query = req.body.query || {};
        //query.published = true;

        var options = {
            sort: req.body.sort || {createdAt: -1},            
            lean: false,
            page: page, 
            limit: perPage,
            populate: populateOption.populate
        };
        
        const data = await Service.paginate(query, options); 
        data.docs.description = data && data.docs && decoder.feed(data.docs.description);

        //console.log("docs:", decoder.feed(data.docs.description));

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};

exports.exportServices = base.export(Service);

exports.advanceServiceSearch = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    };
    try {
        const user = await User.findById(req.body.query['userID']._id);

        if(!user){
            return next(new AppError(404, 'fail', 'This id doesn\'t exist'), req, res, next);
        }

        var page = (req.body.page) ? req.body.page : 1;
        var perPage = (req.body.limit) ? req.body.limit :10;
        var query = {userID: {_id: user._id}, product_name: req.body.query.product_name}

        var options = {
            sort: req.body.sort || {createdAt: -1},      
            populate: populateOption.populate,      
            lean: true,
            page: page, 
            limit: perPage
        };
        
        const data = await Service.paginate(query, options); 

        res.status(200).json({
            status: 'success',
            data
        });

    } catch (error) {
        next(error);
    }
};