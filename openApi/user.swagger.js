exports.getUsers = {
    tags: ['User'],
    summary: "Get All users",
    description: "Returns all users from the system",
    operationId: 'getUsers',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      }
    }
} 

exports.getUser = {
  tags: [
    "User"
  ],
  summary: "Get user by id",
  operationId: "getUserById",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of user that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "success"
    },
    400: {
      description: "error",
    },
    422: {
      description: "validation error message"
    }
  }
}

exports.getUserDocuments = {
  tags: ['User'],
  summary: "Get All user Documents",
  description: "Returns all user documents from the system",
  operationId: 'getUserDocuments',
  security: [
      {
          bearerAuth: []
      }
  ],
  responses: {
    200: {
      description: 'success'
    },
    400: {
      description: 'error'
    }
  }
} 

exports.getUserDocument = {
  tags: [
    "User"
  ],
  summary: "Get single user document",
  operationId: "getUserDocument",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of user document that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "success"
    },
    400: {
      description: "error",
    },
    422: {
      description: "validation error message"
    }
  }
}

exports.forgotPassword = {
  tags: [
    "User"
  ],
  summary: "reset password by email",
  operationId: "forgotPassword",
  parameters: [
    {
      name: "email",
      in: "path",
      description: "The email of user that needs to be fetched. ",
      required: true,
      schema: {
        type: "string"
      }
    }
  ],
  responses: {
    200: {
      description: "A reset link has been sent to your email"
    },
    400: {
      description: "error"
    },
    404: {
      description: "The token provided does not exist"
    },
    403: {
      description: "The email does not exist"
    },
    422: {
      description: "validation error message"
    }
  }
}

exports.postCustomer = {
    tags: [
        'User'
      ],
      summary: "Create Customer",
      description: "Create a new user.",
      operationId: "createUser",
      requestBody: {
        description: "Created customer object",
        content: {
          'application/json': {
            schema: {
              properties:{
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                email: {
                  type: "string"
                },
                password: {
                  type: "string"
                },
                passwordConfirm: {
                  type: "string"
                },
                phone: {
                  type: "string"
                },
                role: {
                  type: "string",
                  enum: [3]
                },
                usertype: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        200: {
          description: 'success'
        },
        400: {
          description: 'error'
        },
        422: {
          description: "validation error message"
        }
    }
}

exports.postAdmin = {
    tags: [
        'User'
      ],
      summary: "Create Admin",
      description: "Create a new user.",
      operationId: "createUser",
      requestBody: {
        description: "Created admin object",
        content: {
          'application/json': {
            schema: {
              properties:{
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                email: {
                  type: "string"
                },
                password: {
                  type: "string"
                },
                passwordConfirm: {
                  type: "string"
                },
                phone: {
                  type: "string"
                },
                role: {
                  type: "string",
                  enum: [1]
                },
                permission: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        200: {
          description: 'success'
        },
        400: {
          description: 'error'
        },
        422: {
          description: "validation error message"
        }
    }
}

exports.postCompany = {
  tags: ['User'],
    summary: "Create Company account",
    description: "Create a new company account.",
    operationId: "createCompany",
    requestBody: {
      description: "Created company object",
      content: {
        'application/json': {
          schema: {
            properties: {
                firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              email: {
                type: "string"
              },
              password: {
                type: "string"
              },
              passwordConfirm: {
                type: "string"
              },
              phone: {
                type: "string"
              },
              role: {
                type: "string",
                enum: ["1", "2"]
              },              
              company_name: {
                type: "string"
              },
              company_address: {
                type: "string"
              },
              company_email: {
                type: "string"
              },
              company_phone: {
                type: "string"
              },
              logo_url: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      404: {
        description: 'This path doesn\'t exist'
      },
      422: {
        description: "validation error message"
      }
  }
}

exports.registerCompanyAdmin = {
  tags: [
      'User'
    ],
    summary: "Create Company admin",
    description: "Create a new company admin.",
    operationId: "createCompanyAdmin",
    requestBody: {
      description: "Created company object",
      content: {
        'application/json': {
          schema: {
            properties:{
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              email: {
                type: "string"
              },
              password: {
                type: "string"
              },
              passwordConfirm: {
                type: "string"
              },
              phone: {
                type: "string"
              },
              role: {
                type: "string",
                enum: [1]
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      404: {
        description: 'This Company Admin doesn\'t exist'
      },
      401: {
        description: 'Unauthorized'
      },
      422: {
        description: 'validation error'
      }
  }
};

exports.exportUser = {
  tags: [
      'User'
    ],
    summary: "export user",
    description: "export user",
    operationId: "exportUser",
    requestBody: {
      description: "example of string : firstname lastname",
      content: {
        'application/json': {
          schema: {
            properties:{
              fields: {
                type: "string"
              },
              role: {
                type: "number"
              },
              parentID: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: 'validation error'
      }
  }
};

exports.getAccountName = {
  tags: [
      'User'
    ],
    summary: "Get account name",
    description: "Get account name from flutterwave",
    operationId: "getAccountName",
    requestBody: {
      description: "get account name object",
      content: {
        'application/json': {
          schema: {
            properties:{
              account_number: {
                type: "string"
              },
              account_bank: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: "validation error message"
      }
  }
};

exports.transfer = {
  tags: [
      'User'
    ],
    summary: "transfer money",
    description: "transfer money to companies from flutterwave",
    operationId: "transfer",
    requestBody: {
      description: "transfer money object",
      content: {
        'application/json': {
          schema: {
            properties:{
              paymentID: {
                type: "string"
              },
              payment_amount: {
                type: "number"
              },
              company_name: {
                type: "string"
              },
              account_number: {
                type: "string"
              },
              account_bank: {
                type: "string"
              },
              amount: {
                type: "number"
              },
              narration: {
                type: "string"
              },
              currency: {
                type: "string"
              },
              reference: {
                type: "string"
              },
              debit_currency: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: "validation error message"
      }
  }
};

exports.advancedUserSearch = {
    tags: [
        'User'
      ],
      summary: "Advanced Search user",
      description: "Search the database.",
      operationId: "advancedUserSearch",
      requestBody: {
        description: "found searched documents",
        content: {
          'application/json': {
            schema: {
              properties: {
                query: {
                  type: "object",
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: "-1, 1"
                    }
                  }
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        200: {
          description: 'success'
        },
        400: {
          description: 'error'
        },
        422: {
          description: 'validation error'
        }
    }
}

exports.searchUser = {
  tags: [
      'User'
    ],
    summary: "Search user",
    description: "Search the database.",
    operationId: "searchUser",
    requestBody: {
      description: "found searched documents",
      content: {
        'application/json': {
          schema: {
            properties: {
              query: {
                type: "object",
                properties: {
                  field: {
                    type: "string"
                  }
                }
              },
              limit : {
                type: "number",
                default: 10
              },
              sort: {
                properties: {
                  field: {
                    type: "number",
                    enum: "-1, 1"
                  }
                }
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      }
  }
}

exports.updateUser = {
  tags: [
    "User"
  ],
  summary: "Update user",
  description: "This can only be done by the logged in user.",
  operationId: "updateUser",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of user that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated user object",
    content: {
      "*/*": {
        schema: {
          properties: {
            _id: {
              type: "string"
            },               
            firstname: {
              type: "string"
            },
            lastname: {
              type: "string"
            },
            email: {
              type: "string"
            },
            password: {
              type: "string"
            },
            passwordConfirm: {
              type: "string"
            },
            phone: {
              type: "string"
            },
            role: {
              type: "number",
              enum: [1, 2, 3]
            },                 
            isActive: {
              type: "boolean",
              description: "User Status"
            }                      
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "updated"
    },
    406: {
      description: "Not Acceptable",
    },
    404: {
      description: "Not found"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
};

exports.kycFormUpload = {
  tags: [
    "User"
  ],
  summary: "Update kyc_form",
  description: "This can only be done by the logged in user.",
  operationId: "kycFormUpload",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of user's kycForm that needs to be uploaded",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated kyc form object",
    content: {
      "application/json": {
        schema: {
          properties: {
            _id: {
              type: "string"
            },
            kyc_form: {
              type: "string"
            }       
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "uploaded"
    },
    400: {
      description: "error"
    },
    401: {
      description: "unauthorized"
    },
    404: {
      description: "Not found"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
};

exports.updateCompany = {
  tags: [
    "User"
  ],
  summary: "Update company",
  description: "This can only be done by the logged in user.",
  operationId: "updateCompany",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of company user that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated document object",
    content: {
      'application/json': {
        schema: {
          properties: {   
            _id: {
              type: "string"
            },            
            firstname: {
              type: "string"
            },
            lastname: {
              type: "string"
            },
            email: {
              type: "string"
            },
            password: {
              type: "string"
            },
            phone: {
              type: "string"
            },
            role: {
              type: "string",
              enum: ["1", "2"]
            },                 
            isActive: {
              type: "boolean",
              description: "User Status"
            },              
            company_name: {
              type: "string"
            },
            company_address: {
              type: "string"
            },
            company_email: {
              type: "string"
            },
            company_phone: {
              type: "string"
            },
            logo_url: {
              type: "string"
            },
            confirmation_token: {
              type: "String"
            },
            remember_token: {
              type: "String"
            },
            children: {
              type: 'array'       
            },
            documents: {
              type: "array"
            },
            userType: {
                type: "String",
            }                                   
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: 'updated'
    },
    400: {
      description: "error"
    },
    404: {
      description: "Not found"
    },
    406: {
      description: "Not Acceptable"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
};

exports.addBankDetails = {
  tags: [
    "User"
  ],
  summary: "add bank details",
  description: "This can only be done by the logged in user.",
  operationId: "addBankDetails",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of company user whose bank details is to be added",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "the request body for the url",
    content: {
      'application/json': {
        schema: {
          properties: {   
            _id: {
              type: "string"
            },            
            bank_id: {
              type: "string"
            },
            account_name: {
              type: "string"
            },
            account_number: {
              type: "number"
            },
            bank_code: {
              type: "string"
            }                       
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: 'updated'
    },
    400: {
      description: "error"
    },
    404: {
      description: "Not found"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
};


exports.documentUpload = {
  tags: ["User"],
  summary: "Update document",
  description: "This can only be done by the logged in user.",
  operationId: "documentUpload",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of user's document that needs to be uploaded",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    },
  ],
  requestBody: {
    description: "Updated document object",
    content: {
      "*/*": {
        schema: {
          properties: {
            _id: {
              type: "string"
            },
            field_name: {
              type: "string"
            }      
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "uploaded"
    },
    400: {
      description: "error",
    },
    404: {
      description: "Not found",
    },
    406: {
      description: "Not Acceptable"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
};

exports.confirmAccount = {
  tags: [
    "User"
  ],
  summary: "activate account",
  description: "This can only be done by the logged in user.",
  operationId: "confirmAccount",
  parameters: [
    {
      name: "random_character",
      in: "path",
      description: "The random_character sent to the user's email, whose account is to be activated. This activates the isActive field to true automatically when it link is loaded.",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated user object",
    content: {
      "*/*": {
        schema: {
          properties: {                 
            isActive: {
              type: "boolean",
              description: "User Status"
            }                      
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "your account has been activated successfully"
    },
    400: {
      description: "error"
    },
    404: {
      description: "This confirmation token doesn\'t exist"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.resetPassword = {
  tags: [
    "User"
  ],
  summary: "reset password",
  description: "Reset User Password.",
  operationId: "resetPassword",
  parameters: [
    {
      name: "random_character",
      in: "path",
      description: "random_character attached to the link sent to the user's email to reset password",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated user object",
    content: {
      "*/*": {
        schema: {
          properties: {               
            password: {
              type: "string"
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "your password reset was successful"
    },
    400: {
      description: "error"
    },
    404: {
      description: "Invalid token"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.changePassword = {
  tags: [
    "User"
  ],
  summary: "Change Password",
  description: "Change user password",
  operationId: "Change Password",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "user's id",
      required: true,
      schema: {
        type: "string",
      }
    }
  ],
  requestBody: {
    description: "Updated user object",
    content: {
      "*/*": {
        schema: {
          properties: {    
            old_password: {
              type: "string"
            },           
            password: {
              type: "string"
            },
            confirm_password: {
              type: "string"
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "your password change was successful"
    },
    400: {
      description: "error"
    },
    404: {
      description: "Invalid token"
    },
    409: {
      description: "Conflict"
    },
    422: {
      description: 'validation error'
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.deleteUser = {
  tags: ["User"],
  summary: "Delete user",
  description: "This can only be done by the logged in user.",
  operationId: "deleteUser",
  parameters: [
    {
      name: "userId",
      in: "path",
      description: "id of user that needs to be deleted",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "This document has been deleted"
    },
    400: {
      description: "error"
    },
    417: {
      description: "Expectation failed"
    },
    404: {
      description: "Not found, no document found with that id"
    }
  }
}

exports.loginUser = {
  tags: [
    "User"
  ],
  summary: "Logs user into the system",
  operationId: "loginUser",
  security: [
        {
            bearerAuth: []
        }
    ],
  parameters: [
    {
      name: "email",
      in: "query",
      description: "The user name for login",
      required: true,
      schema: {
        type: "string"
      }
    },
    {
      name: "password",
      in: "query",
      description: "The password for login in clear text",
      required: true,
      schema: {
        type: "string"
      }
    }
  ],
  responses: {
    200: {
      description: "success"
    },
    400: {
      description: "error"
    },
    401: {
      description: "Unauthorized"
    },
    406: {
      description: "Not accepted"
    },
    422: {
      description: 'validation error'
    }
  }
}

exports.logoutUser = {
  tags: [
    "User"
  ],
  summary: "Logs out current logged in user session",
  operationId: "logoutUser",
  responses: {
    200:{
      description: "you have successfully logged out of your account"
    },
    417: {
      description: "Expectation failed"
    },
    400: {
      description: "error"
    },
    422: {
      description: 'validation error'
    }
  }

}

exports.deleteUploadedFile = {
  tags: [
    "User"
  ],
  summary: "Delete uploaded file",
  description: "This can only be done by the logged in user.",
  operationId: "deleteUploadedFile",
  parameters: [
    {
      name: "user_id",
      in: "path",
      description: "id of user",
      required: true,
      schema: {
        type: "string"
      }
    }
  ],
  requestBody: {
    description: "uploaded file form object",
    content: {
      "*/*": {
        schema: {
          properties: {
            user_id: {
              type: "string"
            },
            file_name: {
              type: "string"
            }       
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "This document has been delected"
    },
    401: {
      description: "Unauthorized"
    },
    417: {
      description: "Expectation failed"
    }
  },
  "x-codegen-request-body-name": "body"
};
