exports.getProducts = {
    tags: ['Product'],
    summary: "Get All Products",
    description: "Returns all Products from the system",
    operationId: 'getProducts',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      '200': {
        description: 'Products were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Products'
            }
          }
        }
      },
      '400': {
        description: 'Missing parameters',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error'
            },
            example: {
              message: 'id is missing',
              internal_code: 'missing_parameters'
            }
          }
        }
      }
    }
} 

exports.getProduct = {
  tags: [
    "Product"
  ],
  summary: "Get Product by id",
  operationId: "getProductById",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the Product that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "successful operation",
      content: {
        "application/xml": {
          schema: {
            $ref: "#/components/schemas/Product"
          }
        },
        "application/json": {
          schema: {
            $ref: "#/components/schemas/Product"
          }
        }
      }
    },
    400: {
      description: "Invalid Product_id supplied",
      content: {}
    },
    404: {
      description: "ID not found",
      content: {}
    }
  }
}

exports.exportProduct = {
  tags: [
      'Product'
    ],
    summary: "export Product",
    description: "export Product",
    operationId: "exportProduct",
    requestBody: {
      description: "example of string : firstname lastname",
      content: {
        'application/json': {
          schema: {
            properties:{
              fields: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: 'validation error'
      }
  }
};

exports.postProduct = {
    tags: [
        'Product'
      ],
      summary: "Create Product",
      description: "Create a new Product detail.",
      operationId: "createProduct",
      requestBody: {
        description: "Created Product object",
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Product'
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'New Product created'
        },
        '400': {
          description: 'Invalid parameters',
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Error'
              },
              example: {
                message: 'User id 10, 20 already exist',
                internal_code: 'invalid_parameters'
              }
            }
          }
        }
    }
}

exports.advancedProductSearch = {
  tags: [
      'Product'
    ],
    summary: "Advanced Search product",
    description: "Search the Product Table.",
    operationId: "advancedProductSearch",
    requestBody: {
      description: "found searched documents",
      content: {
        'application/json': {
          schema: {
            properties: {
              query: {
                type: "object",
                properties: {
                  field: {
                    type: "string"
                  }
                }
              },
              limit : {
                type: "number",
                default: 10
              },
              sort: {
                properties: {
                  field: {
                    type: "number",
                    enum: "-1, 1"
                  }
                }
              },
              select: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      '200': {
        description: 'database search is successful'
      },
      '400': {
        description: 'Invalid parameters',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error'
            },
            example: {
              message: 'Product id 10, 20 already exist',
              internal_code: 'invalid_parameters'
            }
          }
        }
      }
  }
}

exports.searchProduct = {
    tags: [
        'Product'
      ],
      summary: "Search product",
      description: "Search the database.",
      operationId: "searchProduct",
      requestBody: {
        description: "found searched documents",
        content: {
          'application/json': {
            schema: {
              properties: {
                query: {
                  type: "object",
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: "-1, 1"
                    }
                  }
                },
                select: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'database search is successful'
        },
        '400': {
          description: 'Invalid parameters',
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Error'
              },
              example: {
                message: 'Product id 10, 20 already exist',
                internal_code: 'invalid_parameters'
              }
            }
          }
        }
    }
}

exports.updateProduct = {
  tags: [
    "Product"
  ],
  summary: "Update Product",
  description: "This can only be done by the logged in user.",
  operationId: "updateProduct",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of Product that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated Product's object",
    content: {
      "*/*": {
        schema: {
          properties: {  
            _id: {
              type: "string"
            },       
            id: {
              type: "number"
            },      
            name: {
              type: "string"
            },
            description: {
              type: "string"
            },
            parentID: {
              type: "string"
            },
            has_parent: {
              type: "boolean",
              enum: [true, false]
            },
            has_child: {
              type: "boolean",
              enum: [true, false]
            },
            requirement: {
              type: "mixed",
            },
            useParentRequirement: {
              type: "boolean",
              enum: [true, false]
            },
            isActive: {
              type: "boolean",
              description: "Company product Status",
              enum: [true, false]
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    400: {
      description: "Invalid Product's id supplied",
      content: {}
    },
    404: {
      description: "id not found",
      content: {}
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.deleteProduct = {
  tags: [
    "Product"
  ],
  summary: "Delete Product",
  description: "This can only be done by the logged in user.",
  operationId: "deleteProduct",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of Product that needs to be deleted",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    400: {
      description: "Invalid Product's id supplied",
      content: {}
    },
    404: {
      description: "id not found",
      content: {}
    }
  },
}