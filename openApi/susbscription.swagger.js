exports.getSubscriptions = {
    tags: ['Subscription'],
    summary: "Get All subscribers",
    description: "Returns all subscribers from the system",
    operationId: 'getSubscriptions',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      200: {
        description: 'Subscribers were obtained',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Subscriptions'
            }
          }
        }
      },
      400: {
        description: 'error',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Error'
            }
          }
        }
      }
    }
}; 

exports.exportSubscription = {
  tags: [
      'Subscription'
    ],
    summary: "export Subscription",
    description: "export Subscription",
    operationId: "exportSubscription",
    requestBody: {
      description: "example of string : firstname lastname",
      content: {
        'application/json': {
          schema: {
            properties:{
              fields: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      },
      422: {
        description: 'validation error'
      }
  }
};

exports.getSubscription = {
  tags: [
    "Subscription"
  ],
  summary: "Get subscription by id",
  operationId: "getSubscriptionById",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the subscriber that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "success",
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/Subscription"
          }
        }
      }
    },
    400: {
      description: "error"
    },
    404: {
      description: "no document found without that id"
    }
  }
};

exports.getSubscriptionsByCompany = {
  tags: [
    "Subscription"
  ],
  summary: "Get subscription by company id",
  operationId: "getSubscriptionByCompanyId",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the company that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: 'Subscribers were obtained',
      content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/Subscriptions'
          }
        }
      }
    },  
    400: {
      description: "error"
    },
    404: {
      description: "No company document found with that id"
    }
  }
};

exports.downloadSubscriptionDocument = {
  tags: [
    "Subscription"
  ],
  summary: "Download subscription document",
  operationId: "download subscription document",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "subscriptionId",
      required: true,
      schema: {
        type: "string",
      }
    },
    {
      name: "file_name",
      in: "query",
      description: "Document to be downloaded",
      required: true,
      schema: {
        type: "string",
      }
    }
  ],
  responses: {
    200: {
      description: 'success',
      content: {
        'application/json': {
          schema: {
            properties: {
              data_stream: {
                type: 'string'
              }
            }
          }
        }
      }
    },  
    400: {
      description: "error"
    },
    404: {
      description: "There is no subscription with the specified Id"
    },
    403: {
      description: "No file exist with the supplied filename"
    }
  }
};

exports.downloadCertificate = {
  tags: [
    "Subscription"
  ],
  summary: "Download Certificate",
  operationId: "download certificate",
  parameters: [    
    {
      name: "file_name",
      in: "query",
      description: "Document to be downloaded",
      required: true,
      schema: {
        type: "string",
      }
    }
  ],
  responses: {
    200: {
      description: 'success',
      content: {
        'application/json': {
          schema: {
            properties: {
              data_stream: {
                type: 'string'
              }
            }
          }
        }
      }
    },  
    400: {
      description: "error"
    },
    404: {
      description: "There is no subscription with the specified Id"
    },
    403: {
      description: "No file exist with the supplied filename"
    }
  }
};

exports.postSubscription = {
    tags: [
        'Subscription'
      ],
      summary: "Create subscription",
      description: "Create a new subscription detail.",
      operationId: "createSubscription",
      requestBody: {
        description: "Created subscription object",
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Subscription'
            }
          }
        },
        required: true
      },
      responses: {
        200: {
          description: 'success'
        },
        400: {
          description: 'error',
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Error'
              }
            }
          }
        },
        404: {
          description: 'This product is not found'
        },
        403: {
          description: 'This user is not found'
        },
        402: {
          description: 'Your subscription was not successful'
        },
        401: {
          description: 'The document was not uploaded/check the size of the document'
        }
    }
};

exports.advancedSubscriptionSearch = {
  tags: [
      'Subscription'
    ],
    summary: "Advcanced Search Subscription",
    description: "Search the Subscription Table.",
    operationId: "advancedSubscriptionSearch",
    requestBody: {
      description: "found searched documents",
      content: {
        'application/json': {
          schema: {
            properties: {
              query: {
                type: "object",
                properties: {
                  field: {
                    type: "string"
                  }
                }
              },
              limit : {
                type: "number",
                default: 10
              },
              sort: {
                properties: {
                  field: {
                    type: "number",
                    enum: "-1, 1"
                  }
                }
              },
              select: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      200: {
        description: 'success'
      },
      400: {
        description: 'error'
      }
    }
};

exports.searchSubscription = {
    tags: [
        'Subscription'
      ],
      summary: "Search subscription",
      description: "Search the database.",
      operationId: "searchSubscription",
      requestBody: {
        description: "found searched documents",
        content: {
          'application/json': {
            schema: {
              properties: {
                query: {
                  type: "object",
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: "-1, 1"
                    }
                  }
                },
                select: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        200: {
          description: 'success'
        },
        400: {
          description: 'error'
        }
      }
};

exports.updateSubscription = {
  tags: [
    "Subscription"
  ],
  summary: "Update subscription",
  description: "This can only be done by the logged in user.",
  operationId: "updateSubscription",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of subscriber that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated subscriber's object",
    content: {
      "*/*": {
        schema: {
          properties: {   
            _id: {
              type: "string"
            },      
            userID: {
              type: "string"
            },
            serviceID: {
              type: "string"
            },
            startDate: {
              type: "string"
            },
            duration: {
              type: "string",
              enum: ["1", "2"]
            },
            phone: {
              type: "string"
            },
            period: {
              type: "string",
            },
            endDate: {
              type: "string"
            },
            certificateURL: {
              type: "string"
            },   
            isActive: {
              type: "boolean",
              description: "subscription Status",
              enum: [true, false]
            },              
            isPaid: {
              type: "boolean",
              description: "Payment Status",
              enum: [true, false]
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: "updated"
    },
    400: {
      description: "the route id does not match the supplied id",
    },
    404: {
      description: "No document found with that id"
    }
  },
  "x-codegen-request-body-name": "body"
};

exports.uploadCertificate = {
  tags: [
    "Subscription"
  ],
  summary: "Upload certificate",
  description: "This can only be done by the logged in company admin.",
  operationId: "uploadCertificate",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of subscriber whose certificate is to be uploaded",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "Updated subscriber's object",
    content: {
      "*/*": {
        schema: {
          properties: {
            _id: {
              type: "string"
            },
            certificateURL: {
              type: "string"
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    200: {
      description: 'success'
    },
    400: {
      description: "Invalid subscriber's id supplied"
    },
    404: {
      description: "request id does not match the id supplied"
    },
    403: {
      description: "The document was not uploaded, check the size of the document"
    },
    402: {
      description: "No subscription document found with that id"
    },
    401: {
      description: "No user document found with that id"
    }
  },
  "x-codegen-request-body-name": "body"
};

exports.deleteSubscription = {
  tags: [
    "Subscription"
  ],
  summary: "Delete subscriber",
  description: "This can only be done by the logged in user.",
  operationId: "deleteSubscription",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of subscriber that needs to be deleted",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "This document has been deleted"
    },
    400: {
      description: "error"
    },
    404: {
      description: "No document found with that id"
    }
  },
};