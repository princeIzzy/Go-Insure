//
exports.getAllBanks = {
    tags: ['Bank'],
    summary: "Get All Banks",
    description: "Returns all Banks from the system",
    operationId: 'getAllBanks',
    security: [
        {
            bearerAuth: []
        }
    ],
    responses: {
      '200': {
        description: 'success',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Bank'
            }
          }
        }
      },
      '400': {
        description: 'Error'
      }
    }
} 

exports.getBankDetails = {
  tags: [
    "Bank"
  ],
  summary: "Get Bank by id",
  operationId: "getBankDetailsById",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "The id of the Bank that needs to be fetched. ",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    200: {
      description: "success",
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/Bank"
          }
        }
      }
    },
    400: {
      description: "Error"
    },
    404: {
      description: "No document found with that id"
    }
  }
}

exports.createBankDetails = {
    tags: [
        'Bank'
      ],
      summary: "Create Bank",
      description: "Create a new bank detail.",
      operationId: "createBankDetails",
      requestBody: {
        description: "Created Bank object",
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Bank'
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'success'
        },
        '400': {
          description: 'Error'
        }
    }
}

exports.searchBank = {
    tags: [
        'Bank'
      ],
      summary: "Search Bank",
      description: "Search the database.",
      operationId: "searchAccount",
      requestBody: {
        description: "found searched documents",
        content: {
          'application/json': {
            schema: {
              properties: {
                query: {
                  type: "object",
                  properties: {
                    field: {
                      type: "string"
                    }
                  }
                },
                limit : {
                  type: "number",
                  default: 10
                },
                sort: {
                  properties: {
                    field: {
                      type: "number",
                      enum: "-1, 1"
                    }
                  }
                },
                select: {
                  type: "string"
                }
              }
            }
          }
        },
        required: true
      },
      responses: {
        '200': {
          description: 'success'
        },
        '400': {
          description: 'Error'
        }
    }
}

exports.adSearchBank = {
  tags: [
      'Bank'
    ],
    summary: "Search Bank",
    description: "Search the database.",
    operationId: "adSearchAccount",
    requestBody: {
      description: "found searched documents",
      content: {
        'application/json': {
          schema: {
            properties: {
              query: {
                type: "object",
                properties: {
                  field: {
                    type: "string"
                  }
                }
              },
              limit : {
                type: "number",
                default: 10
              },
              sort: {
                properties: {
                  field: {
                    type: "number",
                    enum: "-1, 1"
                  }
                }
              },
              select: {
                type: "string"
              }
            }
          }
        }
      },
      required: true
    },
    responses: {
      '200': {
        description: 'success'
      },
      '400': {
        description: 'Error'
      }
  }
};

exports.updateBank = {
  tags: [
    "Bank"
  ],
  summary: "Update Bank",
  description: "This can only be done by the logged in user.",
  operationId: "updateBank",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of bank that needs to be updated",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  requestBody: {
    description: "updated",
    content: {
      "*/*": {
        schema: {
          properties: { 
            _id: {
              type: "MongoID"
            },
            name: {
              type: "string"
            }
          }
        }
      }
    },
    required: true
  },
  responses: {
    400: {
      description: "Error"
    },
    404: {
      description: "No document found with that id"
    }
  },
  "x-codegen-request-body-name": "body"
}

exports.deleteBank = {
  tags: [
    "Bank"
  ],
  summary: "Delete Bank",
  description: "This can only be done by the logged in user.",
  operationId: "deleteBank",
  parameters: [
    {
      name: "id",
      in: "path",
      description: "id of Bank that needs to be deleted",
      required: true,
      schema: {
        type: "integer",
        format: "int64"
      }
    }
  ],
  responses: {
    400: {
      description: "Error"
    },
    404: {
      description: "No document found with that id"
    }
  },
}