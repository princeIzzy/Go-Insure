var seeder = require('mongoose-seed');
bcrypt = require('bcryptjs');


//const db = "mongodb+srv://GoInsureAdmin:Sp%40rkle321@goinsureprod.wf3k0.mongodb.net/GoInsureProd?retryWrites=true&w=majority";
const db = "mongodb+srv://geebengs:Sp%40rkle321@cluster0.bhr5u.mongodb.net/GoInsureStaging?retryWrites=true&w=majority";
// Connect to MongoDB via Mongoose
seeder.connect(db, function() {
 
  // Load Mongoose models
  seeder.loadModels([
    './models/bank.js',
  ]);
 
  // Clear specified collections
  seeder.clearModels(['BankAccount'], function() {
 
    // Callback to populate DB once collections have been cleared
    seeder.populateModels(banks, function(error, done) {
      if(error) {
          return console.log("seed error", error);
      }

      if(done) {
        return console.log("seed done", done);
      }
      seeder.disconnect();
    });
 
  });
});



var user = [
    {
        'model': 'User',
        'documents' : [
            {
                'firstname': "John",
                "lastname": "Odetoyinbo",
                "email": "odetoyinbo.john@gmail.com",
                "password": 'olufemi',
                "role": 1,
                "isActive": true,
                "userType":"admin"
            }
        ]
    }
]

// Data array containing seed data - documents organized by Model
var banks = [
    {
        'model': 'BankAccount',
        'documents' : [
            {
                "id": 132,
                "code": "560",
                "name": "Page MFBank"
              },
              {
                "id": 133,
                "code": "304",
                "name": "Stanbic Mobile Money"
              },
              {
                "id": 134,
                "code": "308",
                "name": "FortisMobile"
              },
              {
                "id": 135,
                "code": "328",
                "name": "TagPay"
              },
              {
                "id": 136,
                "code": "309",
                "name": "FBNMobile"
              },
              {
                "id": 137,
                "code": "011",
                "name": "First Bank of Nigeria"
              },
              {
                "id": 138,
                "code": "326",
                "name": "Sterling Mobile"
              },
              {
                "id": 139,
                "code": "990",
                "name": "Omoluabi Mortgage Bank"
              },
              {
                "id": 140,
                "code": "311",
                "name": "ReadyCash (Parkway)"
              },
              {
                "id": 141,
                "code": "057",
                "name": "Zenith Bank"
              },
              {
                "id": 142,
                "code": "068",
                "name": "Standard Chartered Bank"
              },
              {
                "id": 143,
                "code": "306",
                "name": "eTranzact"
              },
              {
                "id": 144,
                "code": "070",
                "name": "Fidelity Bank"
              },
              {
                "id": 145,
                "code": "023",
                "name": "CitiBank"
              },
              {
                "id": 146,
                "code": "215",
                "name": "Unity Bank"
              },
              {
                "id": 147,
                "code": "323",
                "name": "Access Money"
              },
              {
                "id": 148,
                "code": "302",
                "name": "Eartholeum"
              },
              {
                "id": 149,
                "code": "324",
                "name": "Hedonmark"
              },
              {
                "id": 150,
                "code": "325",
                "name": "MoneyBox"
              },
              {
                "id": 151,
                "code": "301",
                "name": "JAIZ Bank"
              },
              {
                "id": 152,
                "code": "050",
                "name": "Ecobank Plc"
              },
              {
                "id": 153,
                "code": "307",
                "name": "EcoMobile"
              },
              {
                "id": 154,
                "code": "318",
                "name": "Fidelity Mobile"
              },
              {
                "id": 155,
                "code": "319",
                "name": "TeasyMobile"
              },
              {
                "id": 156,
                "code": "999",
                "name": "NIP Virtual Bank"
              },
              {
                "id": 157,
                "code": "320",
                "name": "VTNetworks"
              },
              {
                "id": 158,
                "code": "221",
                "name": "Stanbic IBTC Bank"
              },
              {
                "id": 159,
                "code": "501",
                "name": "Fortis Microfinance Bank"
              },
              {
                "id": 160,
                "code": "329",
                "name": "PayAttitude Online"
              },
              {
                "id": 161,
                "code": "322",
                "name": "ZenithMobile"
              },
              {
                "id": 162,
                "code": "303",
                "name": "ChamsMobile"
              },
              {
                "id": 163,
                "code": "403",
                "name": "SafeTrust Mortgage Bank"
              },
              {
                "id": 164,
                "code": "551",
                "name": "Covenant Microfinance Bank"
              },
              {
                "id": 165,
                "code": "415",
                "name": "Imperial Homes Mortgage Bank"
              },
              {
                "id": 166,
                "code": "552",
                "name": "NPF MicroFinance Bank"
              },
              {
                "id": 167,
                "code": "526",
                "name": "Parralex"
              },
              {
                "id": 168,
                "code": "035",
                "name": "Wema Bank"
              },
              {
                "id": 169,
                "code": "084",
                "name": "Enterprise Bank"
              },
              {
                "id": 170,
                "code": "063",
                "name": "Diamond Bank"
              },
              {
                "id": 171,
                "code": "305",
                "name": "Paycom"
              },
              {
                "id": 172,
                "code": "100",
                "name": "SunTrust Bank"
              },
              {
                "id": 173,
                "code": "317",
                "name": "Cellulant"
              },
              {
                "id": 174,
                "code": "401",
                "name": "ASO Savings and & Loans"
              },
              {
                "id": 175,
                "code": "030",
                "name": "Heritage"
              },
              {
                "id": 176,
                "code": "402",
                "name": "Jubilee Life Mortgage Bank"
              },
              {
                "id": 177,
                "code": "058",
                "name": "GTBank Plc"
              },
              {
                "id": 178,
                "code": "032",
                "name": "Union Bank"
              },
              {
                "id": 179,
                "code": "232",
                "name": "Sterling Bank"
              },
              {
                "id": 180,
                "code": "076",
                "name": "Skye Bank"
              },
              {
                "id": 181,
                "code": "082",
                "name": "Keystone Bank"
              },
              {
                "id": 182,
                "code": "327",
                "name": "Pagatech"
              },
              {
                "id": 183,
                "code": "559",
                "name": "Coronation Merchant Bank"
              },
              {
                "id": 184,
                "code": "601",
                "name": "FSDH"
              },
              {
                "id": 185,
                "code": "313",
                "name": "Mkudi"
              },
              {
                "id": 186,
                "code": "214",
                "name": "First City Monument Bank"
              },
              {
                "id": 187,
                "code": "314",
                "name": "FET"
              },
              {
                "id": 188,
                "code": "523",
                "name": "Trustbond"
              },
              {
                "id": 189,
                "code": "315",
                "name": "GTMobile"
              },
              {
                "id": 190,
                "code": "033",
                "name": "United Bank for Africa"
              },
              {
                "id": 191,
                "code": "044",
                "name": "Access Bank"
              },
              {
                "id": 567,
                "code": "90115",
                "name": "TCF MFB"
              }
        ]
    }
]
var data = [
    {
        'model': 'Products',
        'documents': [
            {
                "_id": "5f7d85adddd06f14977b2ddf",
                "isActive": true,
                "name": "Professional Indemnity & Director’s Liability",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;i>&lt;strong>Professional indemnity&lt;/strong>&lt;/i> insurance, often referred to as professional &lt;i>&lt;strong>liability insurance&lt;/strong>&lt;/i> or PI insurance, covers legal costs and expenses incurred &lt;i>&lt;strong>in&lt;/strong>&lt;/i> your defence.&lt;/p>",
                "requirement": [
                    {
                        "name": "The nature of the business",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit of liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "The number of employees covered",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T09:09:01.999Z",
                "__v": 0
            },
            {
                "_id": "5f7d84b5ddd06f14977b2dde",
                "isActive": true,
                "name": "Personal Accident/Group Personal Accident",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>The &lt;strong>policy&lt;/strong> is also designed to provide &lt;strong>coverage&lt;/strong> on a 24 hours and worldwide basis and are usually ended to &lt;strong>cover&lt;/strong> medical expenses arising out of &lt;strong>accidents&lt;/strong>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Numbers and categories of employees",
                        "input_type": "File"
                    },
                    {
                        "name": "Estimated Annual Salary for each employee",
                        "input_type": "File"
                    },
                    {
                        "name": "Benefits required e.g. 3 times Annual Salary",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit of medical expenses required for each category of employee",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T09:04:53.646Z",
                "__v": 0
            },
            {
                "_id": "5f7d8455ddd06f14977b2ddd",
                "isActive": true,
                "name": "Oil & Gas",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;i>&lt;strong>Oil&lt;/strong>&lt;/i> and &lt;i>&lt;strong>Gas Insurance&lt;/strong>&lt;/i> is an &lt;i>&lt;strong>insurance policy&lt;/strong>&lt;/i> that covers human life, the environment and property from any accidents and &lt;i>&lt;strong>oil&lt;/strong>&lt;/i> and &lt;i>&lt;strong>gas&lt;/strong>&lt;/i> organization activities.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of Risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "Risk details",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T09:03:17.926Z",
                "__v": 0
            },
            {
                "_id": "5f7d83e6ddd06f14977b2ddc",
                "isActive": true,
                "name": "Occupiers Liability",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Occupiers Liability Insurance&lt;/strong> protects the owner or &lt;strong>occupier&lt;/strong> of the building against legal &lt;strong>liabilities for&lt;/strong> bodily injury, death and loss/damage of property suffered by any user of the premises and third parties, as a result of fire, collapse, storm, earthquake, storm, flood or any allied peril.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "Occupation",
                        "input_type": "Text"
                    },
                    {
                        "name": "Telephone number",
                        "input_type": "Text"
                    },
                    {
                        "name": "Sum insured/Limit of liability",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T09:01:26.100Z",
                "__v": 0
            },
            {
                "_id": "5f7d830cddd06f14977b2ddb",
                "isActive": true,
                "name": "Marine Hull",
                "parentID": {
                    "_id": "5f7d81c5ddd06f14977b2dd9",
                    "isActive": true,
                    "name": "Marine",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Marine insurance&lt;/strong> covers the loss or damage of ships, cargo, terminals, and any transport by which the property is transferred, acquired, or held between the points of origin and the final destination.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the insured",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:52:21.312Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Marine hull insurance&lt;/strong> is an &lt;strong>insurance policy&lt;/strong> specifically designed to provide &lt;strong>coverage&lt;/strong> to water vehicles like a &lt;strong>boat&lt;/strong>, ship, yacht, fishing &lt;strong>boat&lt;/strong>, steamer, etc. A &lt;strong>hull&lt;/strong> means the body of the vessel and that is exactly what is covered by this &lt;strong>insurance policy&lt;/strong>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Particulars of the vessel",
                        "input_type": "File"
                    },
                    {
                        "name": "Value of the vessel",
                        "input_type": "File"
                    },
                    {
                        "name": "Period of cover",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:57:48.177Z",
                "__v": 0
            },
            {
                "_id": "5f7d8227ddd06f14977b2dda",
                "isActive": true,
                "name": "Marine Cargo",
                "parentID": {
                    "_id": "5f7d81c5ddd06f14977b2dd9",
                    "isActive": true,
                    "name": "Marine",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Marine insurance&lt;/strong> covers the loss or damage of ships, cargo, terminals, and any transport by which the property is transferred, acquired, or held between the points of origin and the final destination.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the insured",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:52:21.312Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>Cargo insurance or marine cargo insurance covers and protects the cargo when the ship is actually sailing in the oceanic waters.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "A copy of Proforma Invoice",
                        "input_type": "File"
                    },
                    {
                        "name": "A copy of Form “M”",
                        "input_type": "File"
                    },
                    {
                        "name": "Type of cover",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:53:59.818Z",
                "__v": 0
            },
            {
                "_id": "5f7d81c5ddd06f14977b2dd9",
                "isActive": true,
                "name": "Marine",
                "has_parent": false,
                "has_child": true,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Marine insurance&lt;/strong> covers the loss or damage of ships, cargo, terminals, and any transport by which the property is transferred, acquired, or held between the points of origin and the final destination.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:52:21.312Z",
                "__v": 0
            },
            {
                "_id": "5f7d80d1ddd06f14977b2dd8",
                "isActive": true,
                "name": "Money & Cash –In-Transit",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;i>&lt;strong>Money insurance policy&lt;/strong>&lt;/i> provides &lt;i>&lt;strong>cover for&lt;/strong>&lt;/i> loss of &lt;i>&lt;strong>money&lt;/strong>&lt;/i> in &lt;i>&lt;strong>transit&lt;/strong>&lt;/i> between the &lt;i>&lt;strong>insured's&lt;/strong>&lt;/i> premises, bank and other specified places occasioned by robbery, theft or any other fortuitous cause.&lt;/p>",
                "requirement": [
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Money –in-transit",
                        "input_type": "Text"
                    },
                    {
                        "name": "Money in safe",
                        "input_type": "Text"
                    },
                    {
                        "name": "Money in personal custody",
                        "input_type": "Text"
                    },
                    {
                        "name": "Damage to safes",
                        "input_type": "Text"
                    },
                    {
                        "name": "Total cash in vault",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:48:17.386Z",
                "__v": 0
            },
            {
                "_id": "5f7d8029ddd06f14977b2dd7",
                "isActive": true,
                "name": "Industrial All Risk",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Industrial All Risks&lt;/strong> (IAR) is a comprehensive &lt;strong>insurance policy&lt;/strong> that incorporates Fire, Fire Loss of Profit, Theft and Accidental damage covers. It is subdivided it two sections and it covers &lt;strong>all risks&lt;/strong> not excluded by the &lt;strong>policy&lt;/strong>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Sum insured for each class of insurance",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:45:29.619Z",
                "__v": 0
            },
            {
                "_id": "5f7d7e04ddd06f14977b2dd6",
                "isActive": true,
                "name": "HealthCare Professional Indemnity",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Professional Indemnity Insurance&lt;/strong> protects you from claims if your client holds you responsible &lt;strong>for&lt;/strong> errors, or the failure of your work to perform as promised &lt;strong>in&lt;/strong> your &lt;strong>contract&lt;/strong>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Telephone number",
                        "input_type": "Text"
                    },
                    {
                        "name": "Sum insured/Limit of liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Name of medical centre/Institution",
                        "input_type": "Text"
                    },
                    {
                        "name": "Address",
                        "input_type": "Text"
                    },
                    {
                        "name": "Registration number",
                        "input_type": "File"
                    },
                    {
                        "name": "Number of medical professionals",
                        "input_type": "Text"
                    },
                    {
                        "name": "Number of permanent staff",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:36:20.271Z",
                "__v": 0
            },
            {
                "_id": "5f7d7d5cddd06f14977b2dd5",
                "isActive": true,
                "name": "Goods – In- Transit",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>It's a &lt;strong>policy&lt;/strong> that covers &lt;strong>goods&lt;/strong> against loss or damage while in &lt;strong>transit&lt;/strong>, and when they're being loaded and unloaded.&lt;/p>",
                "requirement": [
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "The nature of goods to be carried",
                        "input_type": "File"
                    },
                    {
                        "name": "Estimated annual carrying",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit per carrying",
                        "input_type": "Text"
                    },
                    {
                        "name": "Mode of conveyance",
                        "input_type": "Text"
                    },
                    {
                        "name": "Scope of cover required",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:33:32.216Z",
                "__v": 0
            },
            {
                "_id": "5f7d7c77ddd06f14977b2dd4",
                "isActive": true,
                "name": "Public Liability & Product Liability",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;i>&lt;strong>Product liability insurance&lt;/strong>&lt;/i> is &lt;i>&lt;strong>for&lt;/strong>&lt;/i> businesses that manufacture products &lt;i>&lt;strong>for&lt;/strong>&lt;/i> sale on the &lt;i>&lt;strong>general&lt;/strong>&lt;/i> market.&lt;/p>",
                "requirement": [
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the product",
                        "input_type": "Text"
                    },
                    {
                        "name": "Limit Any one occurrence/Limit of liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Limit Anyone period of insurance",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:29:43.950Z",
                "__v": 0
            },
            {
                "_id": "5f7d7bfdddd06f14977b2dd3",
                "isActive": true,
                "name": "Fidelity Guarantee",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Fidelity Guarantee Insurance&lt;/strong> provides &lt;strong>coverage&lt;/strong> against any direct financial loss sustained by the employer through acts of fraud, dishonesty, forgery or theft committed by his employees &lt;strong>in&lt;/strong> connection with their occupation and duties.&lt;/p>",
                "requirement": [
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Designation of employees to be covered",
                        "input_type": "File"
                    },
                    {
                        "name": "Number of employees to be covered",
                        "input_type": "Text"
                    },
                    {
                        "name": "Limit any one loss",
                        "input_type": "Text"
                    },
                    {
                        "name": "Aggregate limit anyone period of insurance",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:27:41.995Z",
                "__v": 0
            },
            {
                "_id": "5f7d7b8addd06f14977b2dd2",
                "isActive": true,
                "name": "Employer’s Liability",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;i>&lt;strong>Employers&lt;/strong>&lt;/i>' &lt;i>&lt;strong>liability insurance&lt;/strong>&lt;/i> is vital as a protection from losses caused by a job-related injury or illness not covered by workers' compensation.&lt;/p>",
                "requirement": [
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Categories of employees and their estimated annual earnings",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:25:46.933Z",
                "__v": 0
            },
            {
                "_id": "5f7d7b23ddd06f14977b2dd1",
                "isActive": true,
                "name": "Office Comprehensive",
                "parentID": {
                    "_id": "5f7d7a95ddd06f14977b2dcd",
                    "isActive": true,
                    "name": "Fire",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>Fire insurance is &lt;a href=\"https://www.investopedia.com/articles/insurance/09/property-insurance.asp\">property insurance&lt;/a> that covers damage and losses caused by fire. The purchase of fire insurance in addition to homeowners or property insurance helps to cover the cost of replacement, repair, or reconstruction of property, above the limit set by the property insurance policy. Fire insurance policies typically contain general exclusions, such as &lt;a href=\"https://www.investopedia.com/terms/w/war-risk-insurance.asp\">war&lt;/a>, &lt;a href=\"https://www.investopedia.com/terms/n/nuclear-hazards-clause.asp\">nuclear risks&lt;/a>, and similar perils.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Location of risk",
                            "input_type": "Text"
                        },
                        {
                            "name": "List of inventory – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Valuation report – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured on the buildings/contents",
                            "input_type": "File"
                        },
                        {
                            "name": "Indemnity period",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:21:41.492Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>&lt;strong>Office Comprehensive&lt;/strong>. This type of a &lt;strong>policy&lt;/strong> covers loss or damage to the contents and /or buildings as a result of fire, lightning, explosion and its allied perils. Also included &lt;strong>in&lt;/strong> the &lt;strong>cover&lt;/strong> is burst pipes and overflowing of water tanks, impact, theft or attempted theft and malicious damage.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Location of risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "List of inventory – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Valuation report – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured on the buildings/contents",
                        "input_type": "File"
                    },
                    {
                        "name": "Indemnity period",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:24:03.026Z",
                "__v": 0
            },
            {
                "_id": "5f7d7b04ddd06f14977b2dd0",
                "isActive": true,
                "name": "House Owners/Householders",
                "parentID": {
                    "_id": "5f7d7a95ddd06f14977b2dcd",
                    "isActive": true,
                    "name": "Fire",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>Fire insurance is &lt;a href=\"https://www.investopedia.com/articles/insurance/09/property-insurance.asp\">property insurance&lt;/a> that covers damage and losses caused by fire. The purchase of fire insurance in addition to homeowners or property insurance helps to cover the cost of replacement, repair, or reconstruction of property, above the limit set by the property insurance policy. Fire insurance policies typically contain general exclusions, such as &lt;a href=\"https://www.investopedia.com/terms/w/war-risk-insurance.asp\">war&lt;/a>, &lt;a href=\"https://www.investopedia.com/terms/n/nuclear-hazards-clause.asp\">nuclear risks&lt;/a>, and similar perils.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Location of risk",
                            "input_type": "Text"
                        },
                        {
                            "name": "List of inventory – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Valuation report – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured on the buildings/contents",
                            "input_type": "File"
                        },
                        {
                            "name": "Indemnity period",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:21:41.492Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>&lt;strong>House owners&lt;/strong>/&lt;strong>Householders Insurance policy&lt;/strong> is designed to provide protection against loss or damage to Private dwellings &lt;strong>houses&lt;/strong> and apartment (&lt;strong>owners&lt;/strong> or tenants) and the contents (furniture, electrical appliances, jewellery, etc) within the &lt;strong>house&lt;/strong>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Location of risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "List of inventory – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Valuation report – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured on the buildings/contents",
                        "input_type": "File"
                    },
                    {
                        "name": "Indemnity period",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:23:32.897Z",
                "__v": 0
            },
            {
                "_id": "5f7d7ae1ddd06f14977b2dcf",
                "isActive": true,
                "name": "Fire & special perils",
                "parentID": {
                    "_id": "5f7d7a95ddd06f14977b2dcd",
                    "isActive": true,
                    "name": "Fire",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>Fire insurance is &lt;a href=\"https://www.investopedia.com/articles/insurance/09/property-insurance.asp\">property insurance&lt;/a> that covers damage and losses caused by fire. The purchase of fire insurance in addition to homeowners or property insurance helps to cover the cost of replacement, repair, or reconstruction of property, above the limit set by the property insurance policy. Fire insurance policies typically contain general exclusions, such as &lt;a href=\"https://www.investopedia.com/terms/w/war-risk-insurance.asp\">war&lt;/a>, &lt;a href=\"https://www.investopedia.com/terms/n/nuclear-hazards-clause.asp\">nuclear risks&lt;/a>, and similar perils.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Location of risk",
                            "input_type": "Text"
                        },
                        {
                            "name": "List of inventory – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Valuation report – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured on the buildings/contents",
                            "input_type": "File"
                        },
                        {
                            "name": "Indemnity period",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:21:41.492Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>&lt;strong>Fire and special perils policy&lt;/strong> is an &lt;strong>insurance contract&lt;/strong> that safeguards the &lt;strong>insured&lt;/strong> against unforeseen contingency caused by accidental &lt;strong>fire&lt;/strong>, lightning, explosion/implosion, destruction or damage caused by aerial devices, man made &lt;strong>perils in&lt;/strong> the form of riots, strike etc, natural calamities like storm.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Location of risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "List of inventory – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Valuation report – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured on the buildings/contents",
                        "input_type": "File"
                    },
                    {
                        "name": "Indemnity period",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:22:57.143Z",
                "__v": 0
            },
            {
                "_id": "5f7d7ab5ddd06f14977b2dce",
                "isActive": true,
                "name": "Consequential loss / Business Interruption",
                "parentID": {
                    "_id": "5f7d7a95ddd06f14977b2dcd",
                    "isActive": true,
                    "name": "Fire",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>Fire insurance is &lt;a href=\"https://www.investopedia.com/articles/insurance/09/property-insurance.asp\">property insurance&lt;/a> that covers damage and losses caused by fire. The purchase of fire insurance in addition to homeowners or property insurance helps to cover the cost of replacement, repair, or reconstruction of property, above the limit set by the property insurance policy. Fire insurance policies typically contain general exclusions, such as &lt;a href=\"https://www.investopedia.com/terms/w/war-risk-insurance.asp\">war&lt;/a>, &lt;a href=\"https://www.investopedia.com/terms/n/nuclear-hazards-clause.asp\">nuclear risks&lt;/a>, and similar perils.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Location of risk",
                            "input_type": "Text"
                        },
                        {
                            "name": "List of inventory – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Valuation report – where necessary",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured on the buildings/contents",
                            "input_type": "File"
                        },
                        {
                            "name": "Indemnity period",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:21:41.492Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>These &lt;strong>policies&lt;/strong> compensate a &lt;strong>business for loss&lt;/strong> of revenue after a catastrophic event regardless of physical damage to the property or equipment.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Location of risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "List of inventory – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Valuation report – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured on the buildings/contents",
                        "input_type": "File"
                    },
                    {
                        "name": "Indemnity period",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:22:13.130Z",
                "__v": 0
            },
            {
                "_id": "5f7d7a95ddd06f14977b2dcd",
                "isActive": true,
                "name": "Fire",
                "has_parent": false,
                "has_child": true,
                "useParentRequirement": false,
                "description": "&lt;p>Fire insurance is &lt;a href=\"https://www.investopedia.com/articles/insurance/09/property-insurance.asp\">property insurance&lt;/a> that covers damage and losses caused by fire. The purchase of fire insurance in addition to homeowners or property insurance helps to cover the cost of replacement, repair, or reconstruction of property, above the limit set by the property insurance policy. Fire insurance policies typically contain general exclusions, such as &lt;a href=\"https://www.investopedia.com/terms/w/war-risk-insurance.asp\">war&lt;/a>, &lt;a href=\"https://www.investopedia.com/terms/n/nuclear-hazards-clause.asp\">nuclear risks&lt;/a>, and similar perils.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Location of risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "List of inventory – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Valuation report – where necessary",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured on the buildings/contents",
                        "input_type": "File"
                    },
                    {
                        "name": "Indemnity period",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:21:41.492Z",
                "__v": 0
            },
            {
                "_id": "5f7d79e4ddd06f14977b2dcc",
                "isActive": true,
                "name": "Plant All Risk",
                "parentID": {
                    "_id": "5f7d7928ddd06f14977b2dc7",
                    "isActive": true,
                    "name": "Engineering",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;i>&lt;strong>Engineering Insurance&lt;/strong>&lt;/i> is an insurance &lt;i>&lt;strong>policy&lt;/strong>&lt;/i> that covers a wide range of engineering related risks.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Nature of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Value of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Limit for third party liability",
                            "input_type": "Text"
                        },
                        {
                            "name": "Contract agreement",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T08:15:36.616Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>&lt;strong>This Policy&lt;/strong> (otherwise referred to as Machinery &lt;strong>All Risks Insurance&lt;/strong> but excluding breakdown &lt;strong>risks&lt;/strong>) &lt;strong>cover for plants&lt;/strong> and machinery against unforeseen physical loss or damage that may arise from the occurrence of perils that are not specifically excluded from the &lt;strong>cover&lt;/strong>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Value of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit for third party liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Contract agreement",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:18:44.184Z",
                "__v": 0
            },
            {
                "_id": "5f7d79bfddd06f14977b2dcb",
                "isActive": true,
                "name": "Machinery Breakdown",
                "parentID": {
                    "_id": "5f7d7928ddd06f14977b2dc7",
                    "isActive": true,
                    "name": "Engineering",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;i>&lt;strong>Engineering Insurance&lt;/strong>&lt;/i> is an insurance &lt;i>&lt;strong>policy&lt;/strong>&lt;/i> that covers a wide range of engineering related risks.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Nature of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Value of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Limit for third party liability",
                            "input_type": "Text"
                        },
                        {
                            "name": "Contract agreement",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T08:15:36.616Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>This &lt;strong>policy&lt;/strong> covers all those who own and use different types of plant &lt;strong>machinery&lt;/strong> and mechanical equipment.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Value of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit for third party liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Contract agreement",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:18:07.787Z",
                "__v": 0
            },
            {
                "_id": "5f7d7994ddd06f14977b2dca",
                "isActive": true,
                "name": "Erection All Risk",
                "parentID": {
                    "_id": "5f7d7928ddd06f14977b2dc7",
                    "isActive": true,
                    "name": "Engineering",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;i>&lt;strong>Engineering Insurance&lt;/strong>&lt;/i> is an insurance &lt;i>&lt;strong>policy&lt;/strong>&lt;/i> that covers a wide range of engineering related risks.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Nature of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Value of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Limit for third party liability",
                            "input_type": "Text"
                        },
                        {
                            "name": "Contract agreement",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T08:15:36.616Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>EAR &lt;strong>policies&lt;/strong> are designed to &lt;strong>cover&lt;/strong> the &lt;strong>risk&lt;/strong> of loss arising out of the &lt;strong>erection&lt;/strong> and installation of machinery, plant and steel structures, including physical damage to the &lt;strong>contract&lt;/strong> works.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Value of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit for third party liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Contract agreement",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:17:24.852Z",
                "__v": 0
            },
            {
                "_id": "5f7d797addd06f14977b2dc9",
                "isActive": true,
                "name": "Contractor’s All Risk",
                "parentID": {
                    "_id": "5f7d7928ddd06f14977b2dc7",
                    "isActive": true,
                    "name": "Engineering",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;i>&lt;strong>Engineering Insurance&lt;/strong>&lt;/i> is an insurance &lt;i>&lt;strong>policy&lt;/strong>&lt;/i> that covers a wide range of engineering related risks.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Nature of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Value of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Limit for third party liability",
                            "input_type": "Text"
                        },
                        {
                            "name": "Contract agreement",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T08:15:36.616Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>&lt;strong>This Policy&lt;/strong> is a non-standard &lt;strong>insurance policy&lt;/strong> that provides &lt;strong>coverage for&lt;/strong> property damage and third-party injury or damage claims, the two primary types of &lt;strong>risks&lt;/strong> on construction projects&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Value of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit for third party liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Contract agreement",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:16:58.247Z",
                "__v": 0
            },
            {
                "_id": "5f7d795addd06f14977b2dc8",
                "isActive": true,
                "name": "Computer / Electronics All Risk",
                "parentID": {
                    "_id": "5f7d7928ddd06f14977b2dc7",
                    "isActive": true,
                    "name": "Engineering",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;i>&lt;strong>Engineering Insurance&lt;/strong>&lt;/i> is an insurance &lt;i>&lt;strong>policy&lt;/strong>&lt;/i> that covers a wide range of engineering related risks.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the Assured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Nature of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Value of the plants",
                            "input_type": "File"
                        },
                        {
                            "name": "Limit for third party liability",
                            "input_type": "Text"
                        },
                        {
                            "name": "Contract agreement",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T08:15:36.616Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>This &lt;strong>policy&lt;/strong> is more of an &lt;strong>all&lt;/strong> “&lt;strong>All Risk&lt;/strong>” &lt;strong>cover&lt;/strong>, which compensates the &lt;strong>insured for&lt;/strong> losses &lt;strong>insured&lt;/strong> under Fire & special perils &lt;strong>policy&lt;/strong>, Burglary /Theft and Machinery Breakdown &lt;strong>policies&lt;/strong>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Value of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit for third party liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Contract agreement",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:16:26.650Z",
                "__v": 0
            },
            {
                "_id": "5f7d7928ddd06f14977b2dc7",
                "isActive": true,
                "name": "Engineering",
                "has_parent": false,
                "has_child": true,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;i>&lt;strong>Engineering Insurance&lt;/strong>&lt;/i> is an insurance &lt;i>&lt;strong>policy&lt;/strong>&lt;/i> that covers a wide range of engineering related risks.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Assured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Value of the plants",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit for third party liability",
                        "input_type": "Text"
                    },
                    {
                        "name": "Contract agreement",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:15:36.616Z",
                "__v": 0
            },
            {
                "_id": "5f7d7881ddd06f14977b2dc6",
                "isActive": true,
                "name": "Commercial Vehicle",
                "parentID": {
                    "_id": "5f7d7793ddd06f14977b2dc2",
                    "isActive": true,
                    "name": "Motor",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Motor vehicle insurance&lt;/strong>, also called automotive &lt;strong>insurance&lt;/strong>, a &lt;strong>contract&lt;/strong> by which the insurer assumes the risk of any loss the owner or operator of a &lt;strong>car&lt;/strong> may incur through damage to property or persons as the result of an accident.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured – where necessary",
                            "input_type": "Text"
                        },
                        {
                            "name": "Vehicle inspection",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Particulars of the vehicle",
                            "input_type": "File"
                        },
                        {
                            "name": "Commencement date of cover",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:08:51.548Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>&lt;strong>Commercial auto insurance&lt;/strong> covers damage to a &lt;strong>company vehicle&lt;/strong> and bodily injuries when you or an employee cause an accident. &lt;strong>Commercial auto insurance&lt;/strong> provides five types of &lt;strong>insurance&lt;/strong> protection.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured – where necessary",
                        "input_type": "Text"
                    },
                    {
                        "name": "Vehicle inspection",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Particulars of the vehicle",
                        "input_type": "File"
                    },
                    {
                        "name": "Commencement date of cover",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:12:49.990Z",
                "__v": 0
            },
            {
                "_id": "5f7d7832ddd06f14977b2dc5",
                "isActive": true,
                "name": "Private Motor",
                "parentID": {
                    "_id": "5f7d7793ddd06f14977b2dc2",
                    "isActive": true,
                    "name": "Motor",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Motor vehicle insurance&lt;/strong>, also called automotive &lt;strong>insurance&lt;/strong>, a &lt;strong>contract&lt;/strong> by which the insurer assumes the risk of any loss the owner or operator of a &lt;strong>car&lt;/strong> may incur through damage to property or persons as the result of an accident.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured – where necessary",
                            "input_type": "Text"
                        },
                        {
                            "name": "Vehicle inspection",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Particulars of the vehicle",
                            "input_type": "File"
                        },
                        {
                            "name": "Commencement date of cover",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:08:51.548Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>Motor &lt;i>&lt;strong>vehicle insurance&lt;/strong>&lt;/i>, a &lt;i>&lt;strong>contract&lt;/strong>&lt;/i> by which the insurer assumes the risk of any loss, spent on premiums &lt;i>&lt;strong>for private&lt;/strong>&lt;/i> passenger &lt;i>&lt;strong>auto insurance&lt;/strong>&lt;/i> went to claims.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured – where necessary",
                        "input_type": "Text"
                    },
                    {
                        "name": "Vehicle inspection",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Particulars of the vehicle",
                        "input_type": "File"
                    },
                    {
                        "name": "Commencement date of cover",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:11:30.792Z",
                "__v": 0
            },
            {
                "_id": "5f7d77d5ddd06f14977b2dc4",
                "isActive": true,
                "name": "Motor Trade",
                "parentID": {
                    "_id": "5f7d7793ddd06f14977b2dc2",
                    "isActive": true,
                    "name": "Motor",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Motor vehicle insurance&lt;/strong>, also called automotive &lt;strong>insurance&lt;/strong>, a &lt;strong>contract&lt;/strong> by which the insurer assumes the risk of any loss the owner or operator of a &lt;strong>car&lt;/strong> may incur through damage to property or persons as the result of an accident.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured – where necessary",
                            "input_type": "Text"
                        },
                        {
                            "name": "Vehicle inspection",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Particulars of the vehicle",
                            "input_type": "File"
                        },
                        {
                            "name": "Commencement date of cover",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:08:51.548Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>&lt;strong>Motor trade insurance&lt;/strong> or “&lt;strong>traders insurance&lt;/strong>” gives you full &lt;strong>cover&lt;/strong> if you're buying and selling vehicles either from your home or from business premises.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured – where necessary",
                        "input_type": "Text"
                    },
                    {
                        "name": "Vehicle inspection",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Particulars of the vehicle",
                        "input_type": "File"
                    },
                    {
                        "name": "Commencement date of cover",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:09:57.585Z",
                "__v": 0
            },
            {
                "_id": "5f7d77baddd06f14977b2dc3",
                "isActive": true,
                "name": "Motor Cycle",
                "parentID": {
                    "_id": "5f7d7793ddd06f14977b2dc2",
                    "isActive": true,
                    "name": "Motor",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Motor vehicle insurance&lt;/strong>, also called automotive &lt;strong>insurance&lt;/strong>, a &lt;strong>contract&lt;/strong> by which the insurer assumes the risk of any loss the owner or operator of a &lt;strong>car&lt;/strong> may incur through damage to property or persons as the result of an accident.&lt;/p>",
                    "requirement": [
                        {
                            "name": "Address of the Insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Business of the insured – where necessary",
                            "input_type": "Text"
                        },
                        {
                            "name": "Vehicle inspection",
                            "input_type": "File"
                        },
                        {
                            "name": "Sum insured",
                            "input_type": "Text"
                        },
                        {
                            "name": "Particulars of the vehicle",
                            "input_type": "File"
                        },
                        {
                            "name": "Commencement date of cover",
                            "input_type": "Text"
                        }
                    ],
                    "createdAt": "2020-10-07T08:08:51.548Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>Protect your &lt;i>&lt;strong>motorcycle&lt;/strong>&lt;/i> with quality &lt;i>&lt;strong>coverage&lt;/strong>&lt;/i> from Allstate. A knowledgeable Allstate agent is ready to help you understand what &lt;i>&lt;strong>coverage&lt;/strong>&lt;/i> is best &lt;i>&lt;strong>for&lt;/strong>&lt;/i> you.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured – where necessary",
                        "input_type": "Text"
                    },
                    {
                        "name": "Vehicle inspection",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Particulars of the vehicle",
                        "input_type": "File"
                    },
                    {
                        "name": "Commencement date of cover",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:09:30.710Z",
                "__v": 0
            },
            {
                "_id": "5f7d7793ddd06f14977b2dc2",
                "isActive": true,
                "name": "Motor",
                "has_parent": false,
                "has_child": true,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Motor vehicle insurance&lt;/strong>, also called automotive &lt;strong>insurance&lt;/strong>, a &lt;strong>contract&lt;/strong> by which the insurer assumes the risk of any loss the owner or operator of a &lt;strong>car&lt;/strong> may incur through damage to property or persons as the result of an accident.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the insured – where necessary",
                        "input_type": "Text"
                    },
                    {
                        "name": "Vehicle inspection",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Particulars of the vehicle",
                        "input_type": "File"
                    },
                    {
                        "name": "Commencement date of cover",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:08:51.548Z",
                "__v": 0
            },
            {
                "_id": "5f7d7716ddd06f14977b2dc1",
                "isActive": true,
                "name": "Combined House Owner/Householder",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>This is a &lt;i>&lt;strong>policy&lt;/strong>&lt;/i> to &lt;i>&lt;strong>cover&lt;/strong>&lt;/i> your household contents and includes &lt;i>&lt;strong>coverage for&lt;/strong>&lt;/i> fatal injury to you as the &lt;i>&lt;strong>insured&lt;/strong>&lt;/i>. &lt;i>&lt;strong>Houseowner policy&lt;/strong>&lt;/i> does not &lt;i>&lt;strong>cover&lt;/strong>&lt;/i> loss or damage due to subsidence, landslip, riot, strike and malicious damage.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Sum insured for the buildings/contents",
                        "input_type": "Text"
                    },
                    {
                        "name": "Limit for third party liability",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T08:06:46.969Z",
                "__v": 0
            },
            {
                "_id": "5f7d76c0ddd06f14977b2dc0",
                "isActive": true,
                "name": "Combined Group Pers. Accident/Employer’s liability",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>A &lt;i>&lt;strong>group&lt;/strong>&lt;/i> personal &lt;i>&lt;strong>accident insurance policy&lt;/strong>&lt;/i> has the following characteristics: Issued to a &lt;i>&lt;strong>group&lt;/strong>&lt;/i> of people with similar risk profile;&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Business of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Name and categories of employees",
                        "input_type": "File"
                    },
                    {
                        "name": "Limit of medical expenses",
                        "input_type": "File"
                    },
                    {
                        "name": "Scheme of benefits required",
                        "input_type": "File"
                    },
                    {
                        "name": "Estimated Annual Earnings",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:05:20.259Z",
                "__v": 0
            },
            {
                "_id": "5f7d7607ddd06f14977b2dbf",
                "isActive": true,
                "name": "Combined Fire & Burglary",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>To ensure that you do not suffer from significant losses or damages from the same, AIICO's &lt;i>&lt;strong>Fire & Burglary Insurance&lt;/strong>&lt;/i> gives you comprehensive financial &lt;i>&lt;strong>coverage&lt;/strong>&lt;/i>.&lt;/p>",
                "requirement": [
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the business",
                        "input_type": "Text"
                    },
                    {
                        "name": "Nature of the buildings/contents",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured for the buildings/contents",
                        "input_type": "File"
                    },
                    {
                        "name": "Location of risk",
                        "input_type": "Text"
                    },
                    {
                        "name": "Valuation report /inventory",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T08:02:15.589Z",
                "__v": 0
            },
            {
                "_id": "5f7d755addd06f14977b2dbe",
                "isActive": true,
                "name": "Builders Liability",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>A good &lt;strong>builder liability insurance coverage&lt;/strong> can protect against injuries, accidents, or property damage suffered on the job. Furthermore, &lt;strong>construction&lt;/strong> workers can accidentally damage a property.&lt;/p>",
                "requirement": [
                    {
                        "name": "Occupation",
                        "input_type": "Text"
                    },
                    {
                        "name": "Address",
                        "input_type": "Text"
                    },
                    {
                        "name": "Telephone number",
                        "input_type": "Text"
                    },
                    {
                        "name": "Principal",
                        "input_type": "Text"
                    },
                    {
                        "name": "Project",
                        "input_type": "Text"
                    },
                    {
                        "name": "Maintenance period",
                        "input_type": "Text"
                    },
                    {
                        "name": "Sum insured/Limit of liability",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T07:59:22.188Z",
                "__v": 0
            },
            {
                "_id": "5f7d74bcddd06f14977b2dbd",
                "isActive": true,
                "name": "All Risks",
                "has_parent": false,
                "has_child": false,
                "useParentRequirement": false,
                "description": "&lt;p>This &lt;strong>policy&lt;/strong> is very comprehensive and provides &lt;strong>cover&lt;/strong> to the policyholder against the accidental loss of, or damage to, such items caused by hazards (except &lt;strong>for&lt;/strong> those specifically excluded – e.g. damage caused by wear or tear, gradual operating cause, process of cleaning or ironing, etc)&lt;/p>",
                "requirement": [
                    {
                        "name": "Business of the insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "Address of the Insured",
                        "input_type": "Text"
                    },
                    {
                        "name": "List of items to be covered and the value for each item",
                        "input_type": "File"
                    },
                    {
                        "name": "Sum insured",
                        "input_type": "Text"
                    }
                ],
                "createdAt": "2020-10-07T07:56:44.994Z",
                "__v": 0
            },
            {
                "_id": "5f7d7457ddd06f14977b2dbc",
                "isActive": true,
                "name": "Performance Bond",
                "parentID": {
                    "_id": "5f7d73cfddd06f14977b2db8",
                    "isActive": true,
                    "name": "Bond",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Bond insurance&lt;/strong> is a type of &lt;strong>insurance policy&lt;/strong> that a &lt;strong>bond&lt;/strong> issuer purchases that guarantees the repayment of the principal and all associated interest payments to the bondholders &lt;strong>in&lt;/strong> the event of default.&lt;/p>",
                    "requirement": [
                        {
                            "name": "The nature of contract/bid",
                            "input_type": "Text"
                        },
                        {
                            "name": "Letter of acceptance of offer",
                            "input_type": "File"
                        },
                        {
                            "name": "Letter of request for Bond/Guarantee",
                            "input_type": "File"
                        },
                        {
                            "name": "Copy of a valid letter of Award of Contract",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T07:52:47.751Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>A &lt;strong>performance bond&lt;/strong> is a &lt;strong>surety bond&lt;/strong> issued to &lt;strong>guarantee&lt;/strong> the satisfactory &lt;strong>performance&lt;/strong> of a &lt;strong>contract&lt;/strong>, according to its specifications, by a contractor.&lt;/p>",
                "requirement": [
                    {
                        "name": "The nature of contract/bid",
                        "input_type": "Text"
                    },
                    {
                        "name": "Letter of acceptance of offer",
                        "input_type": "File"
                    },
                    {
                        "name": "Letter of request for Bond/Guarantee",
                        "input_type": "File"
                    },
                    {
                        "name": "Copy of a valid letter of Award of Contract",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T07:55:03.451Z",
                "__v": 0
            },
            {
                "_id": "5f7d743eddd06f14977b2dbb",
                "isActive": true,
                "name": "Counter Indemnity Bond",
                "parentID": {
                    "_id": "5f7d73cfddd06f14977b2db8",
                    "isActive": true,
                    "name": "Bond",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Bond insurance&lt;/strong> is a type of &lt;strong>insurance policy&lt;/strong> that a &lt;strong>bond&lt;/strong> issuer purchases that guarantees the repayment of the principal and all associated interest payments to the bondholders &lt;strong>in&lt;/strong> the event of default.&lt;/p>",
                    "requirement": [
                        {
                            "name": "The nature of contract/bid",
                            "input_type": "Text"
                        },
                        {
                            "name": "Letter of acceptance of offer",
                            "input_type": "File"
                        },
                        {
                            "name": "Letter of request for Bond/Guarantee",
                            "input_type": "File"
                        },
                        {
                            "name": "Copy of a valid letter of Award of Contract",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T07:52:47.751Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>A &lt;strong>counter indemnity&lt;/strong> or &lt;strong>guarantee&lt;/strong> is a &lt;strong>guarantee&lt;/strong> issued by &lt;strong>Insurance Company&lt;/strong> to a bank or to another &lt;strong>insurance company&lt;/strong>. The bank/&lt;strong>insurance company&lt;/strong> transfers the risk to an &lt;strong>insurance company&lt;/strong>&lt;/p>",
                "requirement": [
                    {
                        "name": "The nature of contract/bid",
                        "input_type": "Text"
                    },
                    {
                        "name": "Letter of acceptance of offer",
                        "input_type": "File"
                    },
                    {
                        "name": "Letter of request for Bond/Guarantee",
                        "input_type": "File"
                    },
                    {
                        "name": "Copy of a valid letter of Award of Contract",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T07:54:38.855Z",
                "__v": 0
            },
            {
                "_id": "5f7d7418ddd06f14977b2dba",
                "isActive": true,
                "name": "Bid Bond",
                "parentID": {
                    "_id": "5f7d73cfddd06f14977b2db8",
                    "isActive": true,
                    "name": "Bond",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Bond insurance&lt;/strong> is a type of &lt;strong>insurance policy&lt;/strong> that a &lt;strong>bond&lt;/strong> issuer purchases that guarantees the repayment of the principal and all associated interest payments to the bondholders &lt;strong>in&lt;/strong> the event of default.&lt;/p>",
                    "requirement": [
                        {
                            "name": "The nature of contract/bid",
                            "input_type": "Text"
                        },
                        {
                            "name": "Letter of acceptance of offer",
                            "input_type": "File"
                        },
                        {
                            "name": "Letter of request for Bond/Guarantee",
                            "input_type": "File"
                        },
                        {
                            "name": "Copy of a valid letter of Award of Contract",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T07:52:47.751Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>It is a promise by &lt;strong>surety&lt;/strong> to pay the beneficiary a sum of money if the &lt;strong>performance&lt;/strong> guaranteed &lt;strong>in&lt;/strong> terms of the &lt;strong>bond&lt;/strong> fails to materialize.&lt;/p>",
                "requirement": [
                    {
                        "name": "The nature of contract/bid",
                        "input_type": "Text"
                    },
                    {
                        "name": "Letter of acceptance of offer",
                        "input_type": "File"
                    },
                    {
                        "name": "Letter of request for Bond/Guarantee",
                        "input_type": "File"
                    },
                    {
                        "name": "Copy of a valid letter of Award of Contract",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T07:54:00.072Z",
                "__v": 0
            },
            {
                "_id": "5f7d73f7ddd06f14977b2db9",
                "isActive": true,
                "name": "Advance Payment Bond",
                "parentID": {
                    "_id": "5f7d73cfddd06f14977b2db8",
                    "isActive": true,
                    "name": "Bond",
                    "has_parent": false,
                    "has_child": true,
                    "useParentRequirement": false,
                    "description": "&lt;p>&lt;strong>Bond insurance&lt;/strong> is a type of &lt;strong>insurance policy&lt;/strong> that a &lt;strong>bond&lt;/strong> issuer purchases that guarantees the repayment of the principal and all associated interest payments to the bondholders &lt;strong>in&lt;/strong> the event of default.&lt;/p>",
                    "requirement": [
                        {
                            "name": "The nature of contract/bid",
                            "input_type": "Text"
                        },
                        {
                            "name": "Letter of acceptance of offer",
                            "input_type": "File"
                        },
                        {
                            "name": "Letter of request for Bond/Guarantee",
                            "input_type": "File"
                        },
                        {
                            "name": "Copy of a valid letter of Award of Contract",
                            "input_type": "File"
                        }
                    ],
                    "createdAt": "2020-10-07T07:52:47.751Z",
                    "__v": 0
                },
                "has_parent": true,
                "has_child": false,
                "useParentRequirement": true,
                "description": "&lt;p>An &lt;strong>Advance Payment Bond&lt;/strong> is a &lt;strong>guarantee&lt;/strong>, supplied by the party receiving an &lt;strong>advance payment&lt;/strong>, to the party advancing the &lt;strong>payment&lt;/strong>. It provides that the &lt;strong>advanced&lt;/strong> sum will be returned if the agreement under which the &lt;strong>advance&lt;/strong> was made cannot be fulfilled.&lt;/p>",
                "requirement": [
                    {
                        "name": "The nature of contract/bid",
                        "input_type": "Text"
                    },
                    {
                        "name": "Letter of acceptance of offer",
                        "input_type": "File"
                    },
                    {
                        "name": "Letter of request for Bond/Guarantee",
                        "input_type": "File"
                    },
                    {
                        "name": "Copy of a valid letter of Award of Contract",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T07:53:27.005Z",
                "__v": 0
            },
            {
                "_id": "5f7d73cfddd06f14977b2db8",
                "isActive": true,
                "name": "Bond",
                "has_parent": false,
                "has_child": true,
                "useParentRequirement": false,
                "description": "&lt;p>&lt;strong>Bond insurance&lt;/strong> is a type of &lt;strong>insurance policy&lt;/strong> that a &lt;strong>bond&lt;/strong> issuer purchases that guarantees the repayment of the principal and all associated interest payments to the bondholders &lt;strong>in&lt;/strong> the event of default.&lt;/p>",
                "requirement": [
                    {
                        "name": "The nature of contract/bid",
                        "input_type": "Text"
                    },
                    {
                        "name": "Letter of acceptance of offer",
                        "input_type": "File"
                    },
                    {
                        "name": "Letter of request for Bond/Guarantee",
                        "input_type": "File"
                    },
                    {
                        "name": "Copy of a valid letter of Award of Contract",
                        "input_type": "File"
                    }
                ],
                "createdAt": "2020-10-07T07:52:47.751Z",
                "__v": 0
            }
        ]
    }
];