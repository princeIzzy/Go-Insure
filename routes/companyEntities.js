const express = require('express'),
        router = express.Router(),
        {query} = require('express-validator'),
        auth = require('../controllers/authController'),
        companyEntities = require('../controllers/companyEntities');

router.get('/count', [
    query('id').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, companyEntities.countEntities);

module.exports = router;