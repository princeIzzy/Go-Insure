const express = require('express'),
        router = express.Router(),
        auth = require('../controllers/authController'),
        dashboardEntities = require('../controllers/dashboardEntities');

router.get('/count', auth.protect, dashboardEntities.countEntities);

module.exports = router;