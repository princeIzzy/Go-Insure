const express = require('express'),
        router = express.Router(),
        validator = require('validator'),
        {param, check, query} = require('express-validator'), 
        user = require('../controllers/user'),
        Userschema = require('../models/user'),
        auth = require('../controllers/authController'),
        { logo_upload } = require('../services/upload.js'), 
        user_document = require('../controllers/userDocuments'),
        {permissionConstant} = require('../constants/index'),
        { doc_upload } = require('../services/upload.js'); 

router.post('/company/signup', logo_upload.single('logo_url'), [
        check('firstname', 'please fill in your first name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('lastname', 'please fill in your last name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('email').notEmpty().exists().isEmail().custom(val => Userschema.emailExists(val)).isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('phone', 'please give a valid phone number').notEmpty().matches("^[+0-9 ]+$", "i").withMessage("The field name can only contain + with or without numbers").isNumeric().isLength({ max: 20}).withMessage('you have exceeded the maximum length'),
        check('role', 'enter a valid role for this route').notEmpty().isInt().isIn([2]),
        check('permission').notEmpty().isIn([1,2]).withMessage('it must be a number and must not exceed a digit number'),
        check('company_name').notEmpty().exists().custom(val => Userschema.companyNameExists(val)).isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('company_address').notEmpty().exists().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('company_email').notEmpty().exists().isEmail().custom(val => Userschema.companyEmailExists(val)).isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),        
        check('company_phone', 'please give a valid phone number').optional().isNumeric().isLength({ max: 20 }).withMessage('you have exceeded the maximum length')
], user.registerCompany, auth.restrictTo(permissionConstant.SUPERADMIN, permissionConstant.ADMIN));

router.post('/company/register_admin',  [
        check('firstname', 'please fill in your first name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('lastname', 'please fill in your last name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('email').exists().isEmail().custom(val => Userschema.emailExists(val)).isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('phone', 'please give a valid phone number').notEmpty().matches("^[+0-9 ]+$", "i").withMessage("The field name can only contain + with or without numbers").isNumeric().isLength({ max: 20}).withMessage('you have exceeded the maximum length'),
        check('role', 'enter a valid role for this route').notEmpty().isIn([2]).withMessage('it must be an integer'),
        check('permission').notEmpty().isIn([1,2]).withMessage('it must be a number and must not exceed a digit number'),
        check('parentID', 'please state the ID of the company you are registering to').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('userType', 'please indicate the type of user you want to register').notEmpty().matches("^[company]+$", "i").withMessage('The usertype can only contain lower letters and it must be a company').isLength({max: 10}).withMessage('You have exceeded the maximum number of characters')
], user.registerCompanyAdmin, auth.restrictTo(permissionConstant.ADMIN));

router.post('/customer/signup', [
        check('firstname', 'please fill in your first name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('lastname', 'please fill in your last name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('email').exists().isEmail().custom(val => Userschema.emailExists(val)).matches("^[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$", "i").isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('password').notEmpty().isLength({ min: 6, max: 255}).withMessage('You have exceeded the minimum / maximum number of characters'),
        check('passwordConfirm', 'Passwords do not match').custom((value, {req}) => (value === req.body.password)).isLength({ min: 6, max: 255}).withMessage('You have exceeded the minimum / maximum number of characters'),
        check('phone', 'please give a valid phone number').notEmpty().matches("^[+0-9 ]+$", "i").withMessage("The field name can only contain + with or without numbers").isNumeric().isLength({ max: 20}).withMessage('you have exceeded the maximum length'),
        check('role', 'enter a valid role for this route').notEmpty().not().isString().withMessage('it must be an integer').isIn([3]),
        check('userType', 'please indicate the type of user you want to register').notEmpty().matches("^[customer]+$", "i").withMessage('The usertype can only contain lower letters and it must be a customer').isLength({max: 10}).withMessage('You have exceeded the maximum number of characters')
], auth.signup);

router.post('/admin/signup', [
        check('firstname', 'please fill in your first name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('lastname', 'please fill in your last name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('email').exists().isEmail().custom(val => Userschema.emailExists(val)).isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('phone', 'please give a valid phone number').notEmpty().matches("^[+0-9 ]+$", "i").withMessage("The field name can only contain + with or without numbers").isNumeric().isLength({ max: 20}).withMessage('you have exceeded the maximum length'),
        check('role', 'enter a valid role for this route').notEmpty().not().isString().withMessage('it must be an integer').isIn([1]),
        check('permission').notEmpty().isIn([1,2]).withMessage('it must be a number and must not exceed a digit number'),
        check('userType', 'please indicate the type of user you want to register').notEmpty().matches("^[admin]+$", "i").withMessage('The usertype can only contain lower letters and it must be a admin').isLength({max: 10}).withMessage('You have exceeded the maximum number of characters')
], auth.signup, auth.restrictTo(permissionConstant.SUPERADMIN));

router.post('/login',[
        check('email', 'fill in your email').notEmpty().isEmail().withMessage('add a valid email').isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('password', 'fill in your password').notEmpty().isLength({ min: 6, max: 255}).withMessage('You have exceeded the minimum / maximum number of characters'),
        check('role', 'fill in your user role').not().isString().withMessage('it must be an integer').isInt().isIn([1,2,3]).withMessage('it is a single digit')
], auth.login);

router.post('/getAccountName', [
        check('account_number').notEmpty().isNumeric().withMessage('it must be numeric').isLength({max: 25}).withMessage('you have exceeded the maximum number of characters'),
        check('account_bank').notEmpty().isNumeric().withMessage('it must be numeric').isLength({max: 4}).withMessage('you have exceeded the maximum number of characters')
    ], auth.protect, user.getAccountName);    

// router.post('/transfer', [
//         check('paymentID').notEmpty().isMongoId().withMessage('it must be a mongoId').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
//         check('payment_amount').notEmpty().isNumeric().withMessage('it must be a numeral').isLength({max: 25}).withMessage('you have exceeded the maximum number of characters'),
//         check('company_name').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
//         check('account_number').notEmpty().isNumeric().withMessage('it must be numeric').isLength({max: 25}).withMessage('you have exceeded the maximum number of characters'),
//         check('account_bank').notEmpty().isNumeric().withMessage('it must be numeric').isLength({max: 4}).withMessage('you have exceeded the maximum number of characters'),
//         check('amount').notEmpty().isNumeric().withMessage('it must be a numeral').isLength({max: 25}).withMessage('you have exceeded the maximum number of characters'),
//         check('narration').notEmpty().isLength({max: 255}).withMessage('you have exceeded the maximum number of characters'),
//         check('currency').notEmpty().isUppercase().withMessage('it must be upper case').isLength({max: 4}).withMessage('you have exceeded the maximum number of characters'),
//         check('reference').notEmpty().isLength({max:255}).withMessage('you have exceeded the maximum number of characters'),
//         check('debit_currency').notEmpty().isUppercase().withMessage('it must be upper case').isLength({max: 4}).withMessage('you have exceeded the maximum number of characters')
// ], auth.protect, user.transfer);    

router.post('/search', auth.protect, [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], user.searchUser);

router.post('/advanced_search', auth.protect, [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], user.advanceUserSearch);

router.post('/export', [
        check('fields').isString().withMessage('it must be a string').isLength({max:225}).withMessage('you have the maximum number of charcters'),
        check('role', 'enter a valid role for this route').optional().isNumeric().isIn([1, 2, 3]),
        check('parentID').optional().notEmpty().withMessage('it must be null').isIn(["null"]).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, user.exportUser);

router.get('/logout/', auth.logout);

router.get('/', auth.protect, user.getAllUser);

router.get('/forgot_password', [
        query('email', 'Input an email address').notEmpty().isEmail().withMessage('You have inputted an invalid email address').isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        query('role_id', 'Input your role').notEmpty().withMessage('input a valid role id').isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], user.forgot_password);

router.get('/:id', auth.protect, [
        param('id', 'input a valid mongo ID').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], user.getUser);

router.patch('/:id', auth.protect, [
        param('id', 'input a valid id').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'input your valid id').notEmpty().isMongoId().withMessage('it must be a valid mongoID').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('firstname', 'please fill in your first name').optional().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('lastname', 'please fill in your last name').optional().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('email').optional().isEmail().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('password', 'password should not be lesser than 6 letters nor greater than 15').optional().isLength({ min: 6, max: 255}).withMessage('You have exceeded the minimum / maximum number of characters'),
        check('phone', 'please give a valid phone number').optional().notEmpty().matches("^[+0-9 ]+$", "i").withMessage("The field name can only contain + with or without numbers").isNumeric().isLength({ max: 20}).withMessage('you have exceeded the maximum length'),
        check('role', 'enter a valid role for this route').optional().isNumeric().isIn([1, 2, 3]),
        check('permission').optional().notEmpty().isIn([1,2]).withMessage('the permission must be a number in 1 and 2'),
        check('userType', 'Please indicate the type of user you want to register').optional().matches("^[a-z]+$", "i").withMessage('The usertype can only contain lower letters')
], user.updateUser);

router.patch('/company/:id', auth.protect, logo_upload.single('logo_url'), [
        param('id', 'input a valid id').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'insert your id').notEmpty().isMongoId().withMessage('it must be a valid mongo ID').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('company_name').optional().exists().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('company_address').optional().exists().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('company_email').optional().exists().isEmail().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),        
        check('company_phone', 'please give a valid phone number').optional().isNumeric().isLength({ max: 20}).withMessage('you have exceeded the maximum length')
], user.updateCompany, auth.restrictTo(permissionConstant.SUPERADMIN, permissionConstant.ADMIN));

router.post('/bankDetails/:id', [
        param('id', 'input a valid mongo id').isMongoId().isLength({max:25}).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'insert your id').notEmpty().isMongoId().withMessage('it must be a valid mongo ID').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('bank_id', 'input a valid mongo id').notEmpty().isMongoId().isLength({max:25}).withMessage('You have exceeded the maximum number of characters'),
        check('account_name', 'input your bank account name').notEmpty().isLength({max:255}).withMessage('You have exceeded the maximum number of characters'),
        check('account_number', 'input a valid account number').notEmpty().isInt().withMessage('it must be an integer').isLength({max:20, min:10}).withMessage('You have exceeded the maximum number of characters'),
        check('bank_code', 'insert a valid bank code').notEmpty().isInt().withMessage('it must be an integer').isLength({max:10}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, user.addBankDetail);

router.patch('/delete_file/:user_id', auth.protect, [
        param('user_id', 'give a valid user id').isMongoId().isLength({ max: 25 }).withMessage('You have exceeded the maximum number of characters'),
        check('user_id').notEmpty().isMongoId().withMessage('it must be a valid mongo ID').isLength({ max: 25 }).withMessage('You have exceeded the maximum number of characters'),
        check('field_name', 'please give a valid file_name').exists().isString().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters').matches("^[a-zA-Z ,.'-_]+$", "i").withMessage("The field name can only contain letters")
], user.delete_file);

router.patch('/documentUpload/:id', auth.protect, doc_upload.single("documents"), [
        param('id', 'input a valid UUID').isMongoId().isLength({ max: 25 }).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'please fill in the id field').notEmpty().isMongoId().withMessage('it must be a valid mongo ID').isLength({ max: 25 }).withMessage('You have exceeded the maximum number of characters'),
        check('field_name', 'please fill in the field name').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters').matches("^[a-zA-Z ,.'-_]+$", "i").withMessage("The field name can only contain letters"),
], user.documentUpload);

router.patch('/change_password/:id', [
        param('id', 'input a valid mongo id').isMongoId().isLength({ max: 25 }).withMessage('You have exceeded the minimum / maximum number of characters'),
        check('id', 'id field is required').notEmpty().isMongoId().isLength({ max: 25 }).withMessage('You have exceeded the minimum / maximum number of characters'),
        check('old_password', 'old password field is required').notEmpty().isLength({ max: 255, min: 6}).withMessage('You have exceeded the minimum / maximum number of characters'),
        check('password', 'password should not be less than 6 characters')
        .isLength({ min: 6, max: 255 }).withMessage('You have exceeded the maximum number of characters')        
        .custom((value,{req, loc, path}) => {
                if (value !== req.body.confirm_password) {
                    // trow error if passwords do not match
                    throw new Error("Passwords don't match");
                } else {
                    return value;
                }
        }),
        check('confirm_password', 'input your password').notEmpty().isLength({ max: 255, min: 6}).withMessage('You have exceeded the minimum / maximum number of characters')
], user.change_password);

router.patch('/confirm_account/:random_character', [
        param('random_character', 'You are not authorized to perform this operation, input a valid UUID').isUUID().isLength({ max: 37 }).withMessage('You have exceeded the maximum number of characters')
], user.confirm_account);

router.patch('/reset_password/:random_character', [
        param('random_character', 'enter a valid UUID').isUUID().isLength({ max: 37 }).withMessage('You have exceeded the maximum number of characters'),
        check('password', 'fill in a new password').notEmpty().isLength({ min: 6, max: 255}).withMessage('you have exceeded the minimum / maximum number of characters')
], user.reset_password);

router.delete('/:id', auth.protect, [
        param('id', 'input a valid id').exists().notEmpty().isMongoId().isLength({ max: 25 }).withMessage('You have exceeded the maximum number of characters'),
], user.deleteUser);

router.get('/:id/documents', auth.protect, user_document.getUserDocuments);

router.get('/document/:id', auth.protect, [
    param('id', 'input a valid mongo ID').isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], user_document.getUserDocument);

module.exports = router;