const express = require('express'),
        router = express.Router(),
        {param, check} = require('express-validator'), 
        settlementHistory = require('../controllers/settlementHistory'),
        PaymentSchema = require('../models/payment'),
        SettlementHistorySchema = require('../models/settlementHistory'),
        auth = require('../controllers/authController');

// router.post('/addSettlement', [
//     check('paymentID')
//         .notEmpty().isMongoId().withMessage('it must be a mongo id')
//         .custom(val => PaymentSchema.isValid(val)).withMessage('insert a valid mongoID')
//         .isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
//     check('company_name', 'input the company name').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
//     check('total_amount').notEmpty()
//         .isNumeric().withMessage('it must be numeric')
//         .isLength({max: 255}).withMessage('you have exceeded the maximum number of characters'),
//     check('GoInsure_amount').notEmpty()
//         .isNumeric().withMessage('it must be numeric')
//         .isLength({max: 255}).withMessage('you have exceeded the maximum number of characters'),
//     check('company_amount').notEmpty()
//         .isNumeric().withMessage('it must be numeric')
//         .isLength({max: 255}).withMessage('you have exceeded the maximum number of characters')
// ], auth.protect, settlementHistory.addSettlement);

router.post('/search', [
    check('query').isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
    check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
    check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, settlementHistory.searchSettlementHistory);

router.get('/allSettlement', auth.protect, settlementHistory.allSettlementHistory);

router.post('/export', [
    check('fields').isString().withMessage('it must be a string').isLength({max:225}).withMessage('you have the maximum number of charcters')
], auth.protect, settlementHistory.exportSettlementHistory);

router.get('/:id', [
    param('id').notEmpty()
        .isMongoId().withMessage('it must be a mongo id')
        .custom(val => SettlementHistorySchema.isValid(val)).withMessage('insert a valid mongoID')
        .isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, settlementHistory.getSettlementHistory);

router.patch('/update/:id', [
    param('id').notEmpty()
        .isMongoId().withMessage('it must be a mongo id')
        .custom(val => SettlementHistorySchema.isValid(val)).withMessage('insert a valid mongoID')
        .isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, settlementHistory.updateSettlementHistory);

router.delete('/delete/:id', [
    param('id').notEmpty()
        .isMongoId().withMessage('it must be a mongo id')
        .custom(val => SettlementHistorySchema.isValid(val)).withMessage('insert a valid mongoID')
        .isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, settlementHistory.deleteSettlementHistory);

module.exports = router;