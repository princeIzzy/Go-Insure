const express = require('express'),
        router = express.Router(),
        {check, param} = require('express-validator'),
        auth = require('../controllers/authController'),
        {permissionConstant} = require('../constants/index'),
        product = require('../controllers/product');

router.post('/search', [
        check('query').isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], product.searchProduct);

router.post('/advanced_search', auth.protect, [
        check('query').isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], product.advanceProductSearch);

router.get('/', product.getAllProducts);

router.post('/export', [
        check('fields').isString().withMessage('it must be a string').isLength({max:225}).withMessage('you have the maximum number of charcters')
], auth.protect, product.exportProducts);

router.get('/:id', [
        param('id', 'input valid id').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], product.getProduct);

router.post('/', [
        check('name', 'please fill in the name of the product').notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('description', 'please describe your product').notEmpty().trim(),
        check('requirement', 'input valid requirement').optional().notEmpty(),
        check('parentID', 'please describe your product').optional().notEmpty().isMongoId().withMessage('it must be a valid mongoID').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('has_parent', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters'),
        check('has_child', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters'),
        check('useParentRequirement', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters'),
        check('isActive', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, auth.restrictTo(permissionConstant.SUPERADMIN,permissionConstant.ADMIN),  product.createProduct);

router.patch('/:id', [
        param('id', 'input valid id').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'input your valid id').notEmpty().isMongoId().withMessage('it must be a valid mongoID').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('name', 'please fill in the name of the product').optional().notEmpty().trim().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('description', 'please describe your product').optional().notEmpty(),
        check('requirement', 'please describe your product').optional().notEmpty(),
        check('parentID', 'please describe your product').optional().notEmpty().isMongoId().withMessage('it must be a valid mongoID').isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('has_parent', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters'),
        check('has_child', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters'),
        check('useParentRequirement', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters'),
        check('isActive', 'please describe your product').optional().notEmpty().isBoolean().isLength({max: 5}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, auth.restrictTo(permissionConstant.SUPERADMIN, permissionConstant.ADMIN), product.updateProduct);

router.delete('/:id', auth.protect,  [
        param('id', 'input valid id').notEmpty().isMongoId().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], product.deleteProduct);

module.exports = router;