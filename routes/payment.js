const express = require('express'),
        router = express.Router(),
        {check, param} = require('express-validator'),
        auth = require('../controllers/authController'),
        userSchema = require('../models/user'),
        subscriptionSchema = require('../models/subscription'),
        serviceSchema = require('../models/service'),
        payment = require('../controllers/payment');

router.post('/search', [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')

], payment.searchPayment);

router.post('/advanced_search', auth.protect, [
        check('query').notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('limit').optional().notEmpty().isNumeric().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('sort').optional().notEmpty().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], payment.advancePaymentSearch);

router.get('/', payment.getAllPayments);

router.post('/export', [
        check('fields').isString().withMessage('it must be a string').isLength({max:225}).withMessage('you have the maximum number of charcters')
], auth.protect, payment.exportPayments);

router.get('/:id',  [
        param('id', 'input valid id').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters')
], payment.getPayment);

router.post('/', [
        check('userID', 'This user doesn\'t exist').exists().notEmpty().isMongoId().custom(val => userSchema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('subscriptionID').exists().notEmpty().isMongoId().custom(val => subscriptionSchema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('serviceID').notEmpty().isMongoId().custom(val => serviceSchema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('companyID').notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('amount').notEmpty().isInt().withMessage('it must be digits').isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('period', 'input the period for your subscription').notEmpty().isInt().isLength({max: 100}).withMessage('You have exceeded the maximum number of characters'),
        check('authorizationCode').notEmpty().isLength({max: 20}).withMessage('You have exceeded the maximum number of characters'),
], auth.protect, payment.createPayment);

router.patch('/:id', [
        param('id', 'input a valid UUID').isMongoId().isLength({ max: 25 }).withMessage('You have exceeded the maximum number of characters'),
        check('_id', 'please fill in the _id field').notEmpty().isMongoId().withMessage('it must be a valid mongo ID').isLength({ max: 25 }).withMessage('You have exceeded the maximum number of characters'),
        check('userID', 'This user doesn\'t exist').optional().notEmpty().isMongoId().custom(val => userSchema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('subscriptionID').optional().notEmpty().isMongoId().custom(val => subscriptionSchema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('serviceID').optional().notEmpty().isMongoId().custom(val => serviceSchema.isValid(val)).isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('companyID').optional().notEmpty().isMongoId().isLength({max: 25}).withMessage('You have exceeded the maximum number of characters'),
        check('amount').optional().notEmpty().isInt().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters'),
        check('period', 'input the period for your subscription').optional().notEmpty().isInt().isLength({max: 100}).withMessage('You have exceeded the maximum number of characters'),
        check('authorizationCode').optional().notEmpty().matches("^[a-zA-Z0-9- ]+$", "i").withMessage("The field name can only contain alphanumeric and -").isLength({max: 20}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, payment.updatePayment);

router.delete('/:id',  [
        param('id', 'input valid id').notEmpty().isMongoId().isLength({max: 255}).withMessage('You have exceeded the maximum number of characters')
], auth.protect, payment.deletePayment);

router.get('/company/:userId', [
        param('userId', 'input a valid mongo id').isMongoId()
], payment.getPaymentsByCompany);

module.exports = router;