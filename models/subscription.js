const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2'),
    validator = require('validator');

const subscriptionSchema = new mongoose.Schema({
    userID : {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: 'User'
    },
    user_name: {
        type: String,
        require: true
    },
    _user_email: {
        type: String,
        require: true,
        index: true,
        lowercase: true,
        sparse:true,
        validate: [validator.isEmail, ' Please provide a valid email']
    },
    serviceID: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: 'Service'
    },
    startDate: {
        type: Date,
        require: true
    },
    subscriber_data: {
        type: Map,
        of: String
    },
    documents: {
        type: mongoose.Schema.Types.Mixed,
        require: false
    },    
    // duration:{
    //     type: Number,
    //     require: true,
    //     min: 1,
    //     max: 4,
    //     default: 2
    // },
    period:{
        type: Number,
        require: true
    },
    endDate:{
        type: Date,
        require: true
    },
    key:{
        type: mongoose.Schema.Types.Mixed,
        require: false
    },
    certificateURL:{
        type: mongoose.Schema.Types.Mixed,
        require: false
    },
    certificateSent: {
        type: Boolean,
        require: true,
        default: false
    },
    certificateSentDate: {
        type: Date,
        require: true
    },
    isActive: {
        type: Boolean,
        require: true,
        default: false
    },
    isPaid: {
        type: Boolean,
        require: true,
        default: false
    }
},{
    timestamps: { createdAt: true, updatedAt: false },
},{
    collation: { locale: 'en', strength: 2 }
 });

subscriptionSchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('subscription not found')
       })
    }
}


subscriptionSchema.plugin(mongoosePaginate);
const Subscription = mongoose.model('Subscription', subscriptionSchema);
module.exports = Subscription;