const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2');

const settlementHistorySchema = new mongoose.Schema({
    paymentID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payment'
    },
    company_name: {
        type: String,
        require: true
    },    
    total_amount: {
        type: mongoose.Types.Decimal128,
        require: true
    },
    GoInsure_amount: {
        type: mongoose.Types.Decimal128,
        require: true 
    },
    company_amount: {
        type: mongoose.Types.Decimal128,
        require: true
    },
    settlement_date: {
        type: Date,
        default: Date.now
    }
},{
    timestamps: { createdAt: true, updatedAt: false }
},{
    collation: { locale: 'en', strength: 2 }
 })

settlementHistorySchema.virtual('TotalAmount').get(function() {
    return this.total_amount.$numberDecimal;
});
settlementHistorySchema.virtual('GoInsureAmount').get(function() {
    return this.GoInsure_amount.$numberDecimal;
});
settlementHistorySchema.virtual('CompanyAmount').get(function() {
    return this.company_amount.$numberDecimal;
});

settlementHistorySchema.statics = {
    isValid(id) {
       return this.findById(id)
              .then(result => {
                 if (!result) throw new Error('settlement history not found')
       })
    }
}

settlementHistorySchema.plugin(mongoosePaginate);

const SettlementHistory = mongoose.model('SettlementHistory', settlementHistorySchema);
module.exports = SettlementHistory;