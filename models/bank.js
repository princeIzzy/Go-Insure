const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2');

const bankAccountSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    code: {
        type: String,
        require: true
    }
},{
    timestamps: { createdAt: true, updatedAt: false }
},{
    collation: { locale: 'en', strength: 2 }
 })

bankAccountSchema.plugin(mongoosePaginate);
const BankAccount = mongoose.model('BankAccount', bankAccountSchema);
module.exports = BankAccount;