const Subscription = require('../models/subscription'),
Bugsnag = require('@bugsnag/js'),
Email = require('../services/mail');

exports.handleSubscriptionExpiryJob = async () => {
    var now = new Date();
    now = new Date(now.setDate(now.getDate()));

    Subscription.update({endDate: now}, {$set: {isActive: true}}, {multi: true}).exec(function (err, subscriptions) {
        console.log(subscriptions);
        if(err) {
            Bugsnag.notify(err);
        }        

        Bugsnag.notify(subscriptions);
        return subscriptions;        
    });
}

exports.handleSubscriptionExpiryNotificationJob = async (days) => {
    var now = new Date();
    now = new Date(now.setDate(now.getDate()+days));

    Subscription.find({endDate: now}, function (err, subscriptions) {

        console.log(subscriptions);
        if(err) {
            Bugsnag.notify(err);
        } 

        Bugsnag.notify(subscriptions);
        sendBatchEmail();
    });
}

const sendBatchEmail = () => {
    Bugsnag.notify('Batch email sent');

    return 'Batch Email Sent';
}
